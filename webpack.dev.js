const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const path = require("path");
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    devtool: "source-map", //https://webpack.js.org/configuration/devtool/#development
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                // eslint-disable-line quote-props
                NODE_ENV: JSON.stringify("development")
            }
        }),
        new HtmlWebpackPlugin({
            template: "index.html", // Base template to generate the index.html file
            inject: true // This will inject the bundle.js file into the template
        }),
    ],
    devServer: {
        contentBase: path.join(__dirname, "."),
        compress: true,
        port: 9000
    }
});