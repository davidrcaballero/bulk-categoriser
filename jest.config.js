module.exports = {
  verbose: true,
  collectCoverage: false, // Indicates whether the coverage information should be collected while executing the test
  collectCoverageFrom: ["src/**/*.{tsx,ts}", "!src/Interfaces/**/*", "!src/index.ts", "!**/node_modules/**", "!**/vendor/**"],
  coverageThreshold: { // http://facebook.github.io/jest/docs/en/configuration.html#coveragethreshold-object
    global: {
      branches: 85,
      functions: 85,
      lines: 85,
      statements: 85
    }
  },
  transform: {
    "^.+\\.tsx?$": "<rootDir>/node_modules/ts-jest/preprocessor.js"
  },
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
  moduleFileExtensions: [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json"
  ],
  mapCoverage: true,
  setupTestFrameworkScriptFile: "./enzyme.config.js",
  "snapshotSerializers": ["enzyme-to-json/serializer"],
};