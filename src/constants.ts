// {
//     // GENDER: 'gender',
//     // GENDER: 'gender',
//     gender: {

//     }
// }
export const COLUMNS = {
    QA: 'QA',
    GARMENT_INFO: 'GarmentInfo',
    IMAGE: 'Image',
    MEASUREMENTS: 'Measurements',
    GENDER: 'Gender',
    CATEGORY: 'Category',
    TYPE: 'Type',
    SUBTYPE: 'Subtype', 
    CUT: 'Cut', 
    FIT: 'Fit',
    CATEGORISE: 'Categorise',
    STRETCH: 'Stretch',
    STRETCHEXCEPTION: 'StretchException',
    WAISTBANDRISE: 'WaistbandRise',
    STATUS: 'Status'
};



export const SEPARATORS = {
    IDS: '|',
    NAMES: ' - '
}



// export default Constants;