class Utils {
    static selectCharacters(text: string, numberOfChars: number): string {
        return text.slice(0, numberOfChars);
    }
}

export default Utils;