/* 
 * The store is the first redux file that should be created...
*/


/** 
 * Redux uni-directional data flow
 * ----------------------------------
 * React: Some method was called
 *  -> Action: Dispatch an action so reducers can update state
 *    -> Reducer: Passed current state and action to perform.
 *                Makes a copy of the state and returns it.
 *       -> Store: Updates all connected components use `connect`.
 *          -> React-Redux: Inform React to update UI if necessary
 */

import * as thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux'; // leave comments for each of these, applyMiddleware not 
import rootReducer from '../store/reducers';
import {routerMiddleware} from 'react-router-redux';

import { createHashHistory } from 'history';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
const reduxRouterMiddleware = routerMiddleware(createHashHistory());

const configureStore = (initialState = {}) => {
    
    return createStore(
        rootReducer,
        initialState,
        compose(applyMiddleware(reduxRouterMiddleware, reduxImmutableStateInvariant()))
         // react sling shot for other middleware
    );

    // // console.log(applyMiddleware)
    // // @ts-ignore //TODO remove ignore
    // const composeEnhancers =
    //     typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? // error when "__REDUX_DEVTOOLS_EXTENSION_COMPOSE__" not on window object
    //         window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    //         }) : compose;

    // const rootReducer = combineReducers({
    //     data: reducer
    //     // selected: singleReducer,
    //     // categoriesData: categoriesReducer
    // });
    // const store = createStore(rootReducer);

    // // const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
    // return store;
};

export default configureStore;
