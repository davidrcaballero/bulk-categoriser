import * as thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'; // leave comments for each of these, applyMiddleware not 
import { reducer } from './reducer'; // TODO: rename around domain

const rootReducer = combineReducers({
    data: reducer // the property name used here is the name used to access this state throughout the app TODO: rename
});

export default rootReducer;

// export const configure = (initialState = {}) => {

//     // console.log(applyMiddleware)
//     // @ts-ignore //TODO remove ignore
//     const composeEnhancers =
//         typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? // error when "__REDUX_DEVTOOLS_EXTENSION_COMPOSE__" not on window object
//             window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
//             }) : compose;


//     const store = createStore(rootReducer);

//     // const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
//     return store;
// };