/** 
 * The reducer is the 2nd redux file that should be created
 * Reducers are used to slice up the state found in the `store` into a number of separate functions
 * 
 * 
 */
// TODO: import update?
// TODO: rename reducer to domain
import * as actionTypes from '../actions/actionTypes';

const initialState = {
    config: {},
    products: []
};

const getConfig = (state: any, action: any) => {
    // Redux flow: 2nd thing to be called
    return {
        ...state,
        config: action.config // `action.config` name comes from actionCreators
    };
};

const getProducts = (state: any, action: any) => {
    return {
        ...state,
        products: action.products 
    };
};
const routeLocationDidUpdate = (state: any, action: any) => {
    //console.log('action location in reducer ',action.location)
    return {
        ...state,
        location: action.location 
    };
};
const setGarmentStatus = (state: any, action: any) => {
    //console.log('action location in reducer ',action.location)
    return {
        ...state,
        garmentStatus: action.garmentStatus 
    };
};
const sendRowsUpdate = (state: any, action: any) => {
    //console.log('action location in reducer ',action.location)
    return {
        ...state,
        rowsUpdated: action.updated 
    };
};




/**
 * Reducers are just functions which accepts a state and an action and returns a state
 * @param {state} Object this comes from the store
 * @param {action} {type: string, [key: string]: any} this comes from actionCreators
 */
export const reducer = (state = initialState, action: any) => { // TODO give action a proper type

    // no logic should be done in the reducer only in actionCreator

    // never mutate the state object, always return the same state or a new clone using update
    switch (action.type) {
        case actionTypes.GET_CONFIG: return getConfig(state, action);
        case actionTypes.GET_PRODUCTS: return getProducts(state, action);
        case actionTypes.ROUTE_LOCATION_DID_UPDATE: return routeLocationDidUpdate(state, action);
        case actionTypes.SET_GARMENT_STATUS: return setGarmentStatus(state, action);
        case actionTypes.SEND_ROWS_UPDATED: return sendRowsUpdate(state, action);
        default:
            return state; // always return state
    }
};

