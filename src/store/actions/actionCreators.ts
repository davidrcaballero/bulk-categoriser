/* 
 * The action creator is the 1st step which is passed to reducer
 * IMPORTANT: Actions must have a type property
*/

// TODO: rename to tableActions
import * as actionTypes from './actionTypes';
import { IBulkCatConfig } from '../../interfaces/IBulkCatConfig';
import { IBulkCatProduct } from '../../interfaces/IBulkCatProduct';
const bulkCategorisationConfig: IBulkCatConfig = require('../../localData/bulkCatConfig');
const bulkCategorisationProducts: IBulkCatProduct = require('../../localData/bulkCatProducts');


export const updateProducts = (rows: any) => {
    return {
        type: actionTypes.UPDATE_ROWS,
        // rows:   
    }
}

export const getConfig = () => {
    // Redux flow: 1st thing to be called

    // axios request here

    return {
        type: actionTypes.GET_CONFIG,
        config: bulkCategorisationConfig
    };
}
export const routeLocationDidUpdate = (location:any) => {

    return {
        type: actionTypes.ROUTE_LOCATION_DID_UPDATE,
        location: location
    };
}
export const getProducts = () => {

    // axios request here


    return {
        type: actionTypes.GET_PRODUCTS,
        products: bulkCategorisationProducts
    };
}
export const setGarmentStatus = (garmentStatus:any) => {

    return {
        type: actionTypes.SET_GARMENT_STATUS,
        garmentStatus
    };
}
export const sendRowsUpdate = (updated:any) => {

    return {
        type: actionTypes.SEND_ROWS_UPDATED,
        updated:updated
    };
}


