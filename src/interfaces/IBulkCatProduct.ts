import {IBulkCatConfig, IBulkCatColumnValue, IBulkCatStretchFactor} from './IBulkCatConfig';

export interface IBulkCatProduct {
  StateId: number;
  StateName: string;
  Stretch?: Stretch;
  StretchException: IBulkCatStretchFactor;
  StretchBustException?: StretchBustException;
  StretchWaistException?: StretchBustException;
  StretchHipsException?: StretchBustException;
  WaistbandRise?: number;
  Categorise: IBulkCatColumnValue;
  Id: string;
  ClientId: string;
  ClientName: string;
  ArticleNumber: string;
  CategoryTypeString: string;
  CategoryType: number;
  Gender: IBulkCatColumnValue; // needs to be mapped to {Id: 0, Name: 'Male'}
  ProductAbout: ProductAbout;
  Title: string;
  ProductPageUrl?: string;
  BrandName: string;
  BrandId: number;
  Image?: Image;
  Added: string;
  Modified: string;
  Live?: string;
  Sizes: Size[];
  Type?: IBulkCatColumnValue;
  Subtype?: IBulkCatColumnValue;
  Cut?: IBulkCatColumnValue;
  Fit?: IBulkCatColumnValue;
}

interface Size {
  Id: string;
  Title: string;
  OriginalTitle: string;
  SubTitle?: any;
  CombinedTitle?: any;
  Height: number;
  Neck: number;
  Bust: number;
  Chest: number;
  Hips: number;
  Waist: number;
  InsideLeg: number;
  Thigh: number;
  Waistband: number;
  Calf: number;
  Arms: number;
  ArmsShort: number;
  Ordering: number;
  GroupId?: any;
}

interface Image {
  Id: string;
  MainImage: boolean;
  Src: string;
  Order?: number;
}

interface ProductAbout {
  FabricComposition?: string;
  LiningComposition?: any;
  GarmentComposition?: any;
}

interface StretchBustException {
  Id: string;
  Factor: number;
  FactorLabel: string;
  FactorName: string;
}

interface Stretch {
  Id?: any;
  Factor: number;
  FactorLabel: string;
  FactorName: string;
}

