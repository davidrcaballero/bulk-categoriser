export interface IBulkCategorisationProduct {
  StateId: number;
  StateName: string;
  Stretch?: Stretch;
  StretchBustException?: StretchBustException;
  StretchWaistException?: StretchBustException;
  StretchHipsException?: StretchBustException;
  WaistbandRise?: number;
  Categorise: string;
  Id: string;
  ClientId: string;
  ClientName: string;
  ArticleNumber: string;
  CategoryTypeString: string;
  CategoryType: number;
  Gender: number; // needs to be mapped to {Id: 0, Name: 'Male'}
  ProductAbout: ProductAbout;
  Title: string;
  ProductPageUrl?: string;
  BrandName: string;
  BrandId: number;
  Image?: Image;
  Added: string;
  Modified: string;
  Live?: string;
  Sizes: Size[];
  Type?: Type;
  Subtype?: Type;
  Cut?: Type;
  Fit?: Type;
}

interface Type {
  Id: number;
  Name: string;
}

interface Size {
  Id: string;
  Title: string;
  OriginalTitle: string;
  SubTitle?: any;
  CombinedTitle?: any;
  Height: number;
  Neck: number;
  Bust: number;
  Chest: number;
  Hips: number;
  Waist: number;
  InsideLeg: number;
  Thigh: number;
  Waistband: number;
  Calf: number;
  Arms: number;
  ArmsShort: number;
  Ordering: number;
  GroupId?: any;
}

interface Image {
  Id: string;
  MainImage: boolean;
  Src: string;
  Order?: number;
}

interface ProductAbout {
  FabricComposition?: string;
  LiningComposition?: any;
  GarmentComposition?: any;
}

interface StretchBustException {
  Id: string;
  Factor: number;
  FactorLabel: string;
  FactorName: string;
}

interface Stretch {
  Id?: any;
  Factor: number;
  FactorLabel: string;
  FactorName: string;
}

