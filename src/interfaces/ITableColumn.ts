export interface ITableColumn  {
  key: string;
  name: string;
}