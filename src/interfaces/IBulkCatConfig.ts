export interface IBulkCatConfig {
  ProductStates: IProductState[];
  _Statuses: IBulkCatStatus[];
  _WaistbandRises: IBulkCatColumnValue[];
  StretchFactor: IBulkCatColumnValue[];
  _StretchFactors: IBulkCatStretchFactor[];
  _Categories: IBulkCatCategory[];
  Gender: IBulkCatColumnValue[];
  Types: Types;
  _Types: _Types;
  SubTypes: SubTypes;
  Cut: Cut;
  Fit: Fit;
  CategoriesCombined: ICategoriesCombined[];
}

export interface IBulkCatType {
  Id: number,
  Name: string,
  Category: IBulkCatColumnValue
}

export interface _Types {
  "0": {
    "0": IBulkCatType[],
    "1": IBulkCatType[],
    "2": IBulkCatType[]
  },
  "1": {
    "0": IBulkCatType[],
    "1": IBulkCatType[],
    "2": IBulkCatType[]
  }
}

export interface IBulkCatCategory {
  Id: number,
  Name: string,
  Types: IBulkCatColumnValue[]
}

export interface ICategoriesCombined {
  Gender: number;
  Id: string;
  Name: string;
}

export interface IBulkCatStretchFactor {
  Id: string;
  Name: string;
  Label: string;
}

export interface Fit {
  '44': IBulkCatColumnValue[];
  '49': IBulkCatColumnValue[];
  '50': IBulkCatColumnValue[];
  '54': IBulkCatColumnValue[];
  '57': IBulkCatColumnValue[];
  '58': IBulkCatColumnValue[];
  '60': IBulkCatColumnValue[];
  '63': IBulkCatColumnValue[];
  '66': IBulkCatColumnValue[];
  '67': IBulkCatColumnValue[];
  '68': IBulkCatColumnValue[];
  '69': IBulkCatColumnValue[];
  '72': IBulkCatColumnValue[];
  '73': IBulkCatColumnValue[];
  '74': IBulkCatColumnValue[];
  '75': IBulkCatColumnValue[];
  '76': IBulkCatColumnValue[];
  '77': IBulkCatColumnValue[];
  '79': IBulkCatColumnValue[];
  '80': IBulkCatColumnValue[];
  '85': IBulkCatColumnValue[];
  '88': IBulkCatColumnValue[];
  '89': IBulkCatColumnValue[];
  '90': IBulkCatColumnValue[];
  '93': IBulkCatColumnValue[];
  '94': IBulkCatColumnValue[];
  '96': IBulkCatColumnValue[];
  '97': IBulkCatColumnValue[];
  '99': IBulkCatColumnValue[];
  '100': IBulkCatColumnValue[];
  '102': IBulkCatColumnValue[];
  '103': IBulkCatColumnValue[];
  '104': IBulkCatColumnValue[];
  '105': IBulkCatColumnValue[];
  '107': IBulkCatColumnValue[];
  '108': IBulkCatColumnValue[];
  '109': IBulkCatColumnValue[];
  '114': IBulkCatColumnValue[];
  '116': IBulkCatColumnValue[];
  '117': IBulkCatColumnValue[];
  '119': IBulkCatColumnValue[];
  '120': IBulkCatColumnValue[];
  '121': IBulkCatColumnValue[];
  '122': IBulkCatColumnValue[];
  '123': IBulkCatColumnValue[];
  '124': IBulkCatColumnValue[];
  '126': IBulkCatColumnValue[];
  '140': IBulkCatColumnValue[];
  '141': IBulkCatColumnValue[];
  '142': IBulkCatColumnValue[];
  '144': IBulkCatColumnValue[];
  '145': IBulkCatColumnValue[];
  '146': IBulkCatColumnValue[];
  '147': IBulkCatColumnValue[];
  '148': IBulkCatColumnValue[];
  '149': IBulkCatColumnValue[];
  '150': IBulkCatColumnValue[];
  '151': IBulkCatColumnValue[];
  '152': IBulkCatColumnValue[];
  '153': IBulkCatColumnValue[];
  '154': IBulkCatColumnValue[];
  '162': IBulkCatColumnValue[];
  '163': IBulkCatColumnValue[];
  '164': IBulkCatColumnValue[];
  '165': IBulkCatColumnValue[];
  '166': IBulkCatColumnValue[];
  '169': IBulkCatColumnValue[];
  '170': IBulkCatColumnValue[];
  '171': IBulkCatColumnValue[];
  '172': IBulkCatColumnValue[];
  '173': IBulkCatColumnValue[];
  '175': IBulkCatColumnValue[];
  '176': IBulkCatColumnValue[];
  '177': IBulkCatColumnValue[];
  '178': IBulkCatColumnValue[];
  '179': IBulkCatColumnValue[];
  '180': IBulkCatColumnValue[];
  '181': IBulkCatColumnValue[];
  '182': IBulkCatColumnValue[];
  '183': IBulkCatColumnValue[];
  '184': IBulkCatColumnValue[];
  '185': IBulkCatColumnValue[];
  '187': IBulkCatColumnValue[];
  '188': IBulkCatColumnValue[];
  '189': IBulkCatColumnValue[];
  '190': IBulkCatColumnValue[];
  '192': IBulkCatColumnValue[];
  '197': IBulkCatColumnValue[];
  '199': IBulkCatColumnValue[];
  '200': IBulkCatColumnValue[];
  '201': IBulkCatColumnValue[];
  '203': IBulkCatColumnValue[];
  '205': IBulkCatColumnValue[];
  '206': IBulkCatColumnValue[];
  '208': IBulkCatColumnValue[];
  '209': IBulkCatColumnValue[];
  '211': IBulkCatColumnValue[];
  '212': IBulkCatColumnValue[];
  '214': IBulkCatColumnValue[];
  '216': IBulkCatColumnValue[];
  '217': IBulkCatColumnValue[];
  '218': IBulkCatColumnValue[];
  '219': IBulkCatColumnValue[];
  '220': IBulkCatColumnValue[];
  '221': IBulkCatColumnValue[];
  '222': IBulkCatColumnValue[];
  '223': IBulkCatColumnValue[];
  '224': IBulkCatColumnValue[];
  '225': IBulkCatColumnValue[];
  '228': IBulkCatColumnValue[];
  '229': IBulkCatColumnValue[];
  '230': IBulkCatColumnValue[];
  '233': IBulkCatColumnValue[];
  '235': IBulkCatColumnValue[];
  '236': IBulkCatColumnValue[];
  '237': IBulkCatColumnValue[];
  '238': IBulkCatColumnValue[];
  '239': IBulkCatColumnValue[];
  '240': IBulkCatColumnValue[];
  '241': IBulkCatColumnValue[];
  '242': IBulkCatColumnValue[];
  '243': IBulkCatColumnValue[];
  '885': IBulkCatColumnValue[];
  '892': IBulkCatColumnValue[];
  '897': IBulkCatColumnValue[];
  '900': IBulkCatColumnValue[];
  '903': IBulkCatColumnValue[];
  '906': IBulkCatColumnValue[];
  '909': IBulkCatColumnValue[];
  '912': IBulkCatColumnValue[];
  '916': IBulkCatColumnValue[];
  '919': IBulkCatColumnValue[];
  '923': IBulkCatColumnValue[];
  '926': IBulkCatColumnValue[];
  '929': IBulkCatColumnValue[];
  '932': IBulkCatColumnValue[];
  '935': IBulkCatColumnValue[];
  '938': IBulkCatColumnValue[];
  '941': IBulkCatColumnValue[];
  '944': IBulkCatColumnValue[];
  '950': IBulkCatColumnValue[];
  '955': IBulkCatColumnValue[];
  '958': IBulkCatColumnValue[];
  '961': IBulkCatColumnValue[];
  '967': IBulkCatColumnValue[];
  '972': IBulkCatColumnValue[];
  '975': IBulkCatColumnValue[];
  '979': IBulkCatColumnValue[];
  '985': IBulkCatColumnValue[];
  '988': IBulkCatColumnValue[];
  '989': IBulkCatColumnValue[];
  '1038': IBulkCatColumnValue[];
  '1042': IBulkCatColumnValue[];
  '1043': IBulkCatColumnValue[];
  '1053': IBulkCatColumnValue[];
  '1058': IBulkCatColumnValue[];
  '1062': IBulkCatColumnValue[];
  '1067': IBulkCatColumnValue[];
  '1069': IBulkCatColumnValue[];
  '1083': IBulkCatColumnValue[];
  '1087': IBulkCatColumnValue[];
  '1090': IBulkCatColumnValue[];
  '1093': IBulkCatColumnValue[];
  '1097': IBulkCatColumnValue[];
  '1100': IBulkCatColumnValue[];
  '1104': IBulkCatColumnValue[];
  '1107': IBulkCatColumnValue[];
  '1114': IBulkCatColumnValue[];
  '1118': IBulkCatColumnValue[];
  '1119': IBulkCatColumnValue[];
  '1125': IBulkCatColumnValue[];
  '1129': IBulkCatColumnValue[];
}

export interface Cut {
  '11': IBulkCatColumnValue[];
  '12': IBulkCatColumnValue[];
  '13': IBulkCatColumnValue[];
  '14': IBulkCatColumnValue[];
  '16': IBulkCatColumnValue[];
  '17': IBulkCatColumnValue[];
  '18': IBulkCatColumnValue[];
  '19': IBulkCatColumnValue[];
  '20': IBulkCatColumnValue[];
  '21': IBulkCatColumnValue[];
  '22': IBulkCatColumnValue[];
  '23': IBulkCatColumnValue[];
  '25': IBulkCatColumnValue[];
  '26': IBulkCatColumnValue[];
  '27': IBulkCatColumnValue[];
  '29': IBulkCatColumnValue[];
  '30': IBulkCatColumnValue[];
  '31': IBulkCatColumnValue[];
  '32': IBulkCatColumnValue[];
  '33': IBulkCatColumnValue[];
  '34': IBulkCatColumnValue[];
  '35': IBulkCatColumnValue[];
  '36': IBulkCatColumnValue[];
  '37': IBulkCatColumnValue[];
  '38': IBulkCatColumnValue[];
  '39': IBulkCatColumnValue[];
  '40': IBulkCatColumnValue[];
  '41': IBulkCatColumnValue[];
  '42': IBulkCatColumnValue[];
  '43': IBulkCatColumnValue[];
  '1096': IBulkCatColumnValue[];
  '1099': IBulkCatColumnValue[];
  '1117': IBulkCatColumnValue[];
  '1128': IBulkCatColumnValue[];
}

export interface SubTypes {  // TODO BE: match property name in products: Subtype is one word 
  '1': IBulkCatColumnValue[];
  '2': IBulkCatColumnValue[];
  '3': IBulkCatColumnValue[];
  '4': IBulkCatColumnValue[];
  '5': IBulkCatColumnValue[];
  '6': IBulkCatColumnValue[];
  '7': IBulkCatColumnValue[];
  '8': IBulkCatColumnValue[];
  '9': IBulkCatColumnValue[];
  '10': IBulkCatColumnValue[];
}

export interface Types {
  [key: string]: IBulkCatColumnValue[];
  '0': IBulkCatColumnValue[];
  '1': IBulkCatColumnValue[];
}

export interface IBulkCatColumnValue {
  Id: number | string;
  Name: string;
}

export interface IBulkCatStatus {
  Id: number;
  Name: string;
}

export interface IProductState {
  Id: number;
  Title: string;
}