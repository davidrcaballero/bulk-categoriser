import { IReduxDevTools } from './interfaces/IReduxDevTools';

declare global {
    interface Window { __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any; } // install chrome redux extension
}