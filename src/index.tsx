import * as React from "react";
import * as ReactDOM from "react-dom";
import App from './Components/App';
// import './assets/css/main.css';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import * as mui from 'material-ui';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';

/**
 * The  Provider a HoC which is passed the store and makes 
 * it available to the entire app (using the Context)
 */

import configureStore from './store/configureStore';
import { getConfig, getProducts } from "./store/actions";


const store = configureStore();
store.dispatch(getConfig());
store.dispatch(getProducts());

const theme = createMuiTheme({

});

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <MuiThemeProvider theme={theme}>
                <App />
            </MuiThemeProvider>
        </BrowserRouter>
    </Provider>
);
ReactDOM.render( app, document.getElementById('app') );
