import { IBulkCatColumnValue, IBulkCatConfig, IBulkCatStatus, IBulkCatCategory, IBulkCatType } from "../interfaces/IBulkCatConfig";
import { IBulkCatProduct } from "../interfaces/IBulkCatProduct";
import { configure } from "enzyme";
import { COLUMNS, SEPARATORS } from "../constants";
// const update = require('immutability-helper'); 

// TODO: StateId and StateName need to be transformed into ProductStates: {Id: 1, Title: "To be processed"}
// TODO: rename bulk stuff to IBulkCategorisation... to IBulkCat...
// TODO: should be Cuts and Fits coming from server

export interface ICellUpdate {
  rows: IBulkCatProduct[],
  updated: any,
  config: IBulkCatConfig,
  cellKey: string
}

class RowUpdater {
  private _categorisedStatus: IBulkCatStatus;
  private _toBeProcessedStatus: IBulkCatStatus;
  private _waistbandNone: IBulkCatStatus;
  private config: IBulkCatConfig; // TODO: remove config

  constructor(config: IBulkCatConfig) {
    this.config = config;
  }

  get categorisedStatus(): IBulkCatStatus {
    const categorisedStatusId = 18; // TODO: move to config
    return this._categorisedStatus ||
      this.config._Statuses.find((s: IBulkCatStatus) => s.Id === categorisedStatusId);
  }

  get toBeProcessedStatus(): IBulkCatStatus {
    const toBeProcessedStatusId = 1; // TODO: move to config
    return this._toBeProcessedStatus ||
      this.config._Statuses.find((s: IBulkCatStatus) => s.Id === toBeProcessedStatusId);
  }

  get waistbandNone(): IBulkCatStatus {
    const waistbandNoneId = 0; // TODO: move to config
    return this._waistbandNone ||
      this.config._WaistbandRises.find((wbr: IBulkCatColumnValue) => wbr.Id === waistbandNoneId);
  }

  updateRow(row: any, cellUpdate: ICellUpdate) {
    const { updated, cellKey, config } = cellUpdate;
    const args = [row, updated, config, cellKey];

    const { GENDER, CATEGORY, TYPE, SUBTYPE, CUT, FIT, CATEGORISE, WAISTBANDRISE,
      STRETCH, STRETCHEXCEPTION, STATUS } = COLUMNS;

    // TODO: refactor 
    switch (cellKey) {
      case GENDER: return this.updateGender.apply(this, args);
      case CATEGORY: return this.updateCategory.apply(this, args);
      case TYPE: return this.updateType.apply(this, args);
      case SUBTYPE: return this.updateSubtype.apply(this, args);
      case CUT: return this.updateCut.apply(this, args);
      case FIT: return this.updateFit.apply(this, args);
      case CATEGORISE: return this.updateCategorise.apply(this, args);
      case STRETCH: return this.updateStretch.apply(this, args);
      case STRETCHEXCEPTION: return this.updateStretchException.apply(this, args);
      default: return this.updateDefault(row, updated, cellKey);
    }
  }

  updateRowsFromTo(rows: IBulkCatProduct[], cellUpdate: ICellUpdate,
    fromRow: number, toRow: number, selectedIndexes: number[], rowIdx: number) {
    const { updated, cellKey } = cellUpdate;

    const rowToUpdate = rows[rowIdx];

    const templateRow = this.getTemplateRow(rows, cellUpdate, rowIdx);

    const updatableColumnList = this.getCellsToUpdate(cellKey);

    for (let i = fromRow; i <= toRow; i += 1) {
      if (i !== rowIdx) {

        if (this.isRowFromSelectedIndexes(selectedIndexes, i) || selectedIndexes.length === 0) {
          rows[i] = this.updatePropertiesFromList(rows[i], templateRow, updatableColumnList);
          rows[i] = this.updateStatus(rows[i], null);
        }
      }
    }



    return rows;
  }

  updateRowsBySelectedIndexes(cellUpdate: ICellUpdate, selectedIndexes: number[], rowIdx: number, cellKey: string): IBulkCatProduct[] {
    const { rows } = cellUpdate;

    const otherSelectedIndexes = selectedIndexes.filter((i: number) => i !== rowIdx);

    const templateRow = rows[rowIdx] = this.getTemplateRow(rows, cellUpdate, rowIdx);

    const updatableColumnList = this.getCellsToUpdate(cellKey);
    otherSelectedIndexes.map((i: number) => {
      const rowToUpdate = rows[i];

      rows[i] = this.updatePropertiesFromList(rowToUpdate, templateRow, updatableColumnList);
      rows[i] = this.updateStatus(rows[i], null);
    });

    return rows;
  }
  // TODO refactor into objects and groups {[cellKey]: list}
  getCellsToUpdate(cellKey: string): string[] {
    const { GENDER, CATEGORY, TYPE, SUBTYPE, CUT, FIT, CATEGORISE, WAISTBANDRISE,
      STRETCH, STRETCHEXCEPTION, STATUS } = COLUMNS;
    switch (cellKey) {
      case GENDER:
      case CATEGORY:
      case TYPE:
      case SUBTYPE:
      case CUT:
      case FIT:
      case CATEGORISE: return [GENDER, CATEGORY, TYPE, SUBTYPE, CUT, FIT, CATEGORISE];
      case WAISTBANDRISE: return [WAISTBANDRISE];
      case STRETCH: return [STRETCH, STRETCHEXCEPTION];
      case STRETCHEXCEPTION: return [STRETCHEXCEPTION];
      case STATUS: return [STATUS];
      default: return [];
    }
  }

  isRowFromSelectedIndexes(selectedIndexes: number[], rowIdx: number): boolean {
    return selectedIndexes.indexOf(rowIdx) > -1;
  }

  updateDefault(row: IBulkCatProduct, updated: any, cellKey: string): IBulkCatProduct {
    return {
      ...row,
      [cellKey]: updated[cellKey]
    };
  }

  updateGender(row: IBulkCatProduct, updated: any, config: IBulkCatConfig): IBulkCatProduct {
    const updatedRow = {
      ...row,
      Gender: updated.Gender
    };

    return this.updateCategory(updatedRow, null, config);
  }

  updateCategory(row: IBulkCatProduct, updated: any, config: IBulkCatConfig): IBulkCatProduct {
    let updatedRow: any = { ...row }; // TODO remove any

    if (updated) { updatedRow.Category = updated.Category }

    updatedRow = this.updateWaistbandRise(updatedRow, null);

    return this.updateType(updatedRow, null, config);
  }

  updateWaistbandRise(row: IBulkCatProduct, updated: any): IBulkCatProduct {
    let updatedRow: any = {
      ...row
    };

    const categoryUpperId = 0;

    // @ts-ignore
    if (!row.Category || row.Category.Id === categoryUpperId) {
      updatedRow.WaistbandRise = null;
    } else if (!updatedRow.WaistbandRise) {
      updatedRow.WaistbandRise = this.waistbandNone;
    }

    return updatedRow;
  }

  updateType(row: IBulkCatProduct, updated: any, config: IBulkCatConfig): IBulkCatProduct {
    const { CATEGORY, GENDER, TYPE } = COLUMNS;
    const updatedRow = this.updateRowByColumn(row, updated, config, "_TypesByCategory", TYPE, GENDER, CATEGORY);
    return this.updateSubtype(updatedRow, null, config);
  }

  updateSubtype(row: IBulkCatProduct, updated: any, config: IBulkCatConfig): IBulkCatProduct {
    const { SUBTYPE, TYPE } = COLUMNS;

    const updatedRow = this.updateRowByColumn(row, updated, config, "SubTypes", SUBTYPE, TYPE);
    return this.updateCut(updatedRow, null, config);
  }

  updateCut(row: IBulkCatProduct, updated: any, config: IBulkCatConfig): IBulkCatProduct {
    const updatedRow = this.updateRowByColumn(row, updated, config, "Cut", "Cut", "Subtype");
    return this.updateFit(updatedRow, null, config);
  }

  updateFit(row: IBulkCatProduct, updated: any, config: IBulkCatConfig): IBulkCatProduct {
    const updatedRow = this.updateRowByColumn(row, updated, config, "Fit", "Fit", "Cut");
    // return this.updateCut(updatedRow, null, config);
    return this.updateCategorise(updatedRow, null, config);
  }

  updateCategoryByType(row: IBulkCatProduct): IBulkCatProduct {
    const { Type: typeValue } = row; // TODO: fix names

    if (!typeValue) { return row; }

    return {
      ...row,
      // @ts-ignore
      Category: typeValue.Category
    };
  }

  buildColumnValue(id: string, name: string, cellKey: string, genderId: number): IBulkCatColumnValue | IBulkCatType {
    let value: any = {
      Id: id,
      Name: name.trim()
    };

    if (cellKey === COLUMNS.TYPE) {
      // @ts-ignore
      value = this.config._Types[genderId].find((t: IBulkCatType) => String(t.Id) === id);
    } else {
      value = {
        Id: id,
        Name: name.trim()
      };
    }

    return value;
  }

  // TODO: function is too long (DC)
  updateCategorise(row: IBulkCatProduct, updated: any, config: IBulkCatConfig): IBulkCatProduct {
    const { TYPE, SUBTYPE, CUT, FIT } = COLUMNS;

    let updatedRow: IBulkCatProduct;
    const columnList: Array<string> = [TYPE, SUBTYPE, CUT, FIT];

    if (!updated) {
      const categoriseArray = [];

      for (const key in row) {
        if (row.hasOwnProperty(key)) {
          // @ts-ignore (Sergio)
          if (columnList.indexOf(key) > -1 && row[key]) {
            // @ts-ignore (Sergio)
            categoriseArray.push(row[key].Id);
          }
        }
      }

      const categoriseId = categoriseArray.join("|");

      const categorise = config.CategoriesCombined.find((c: any) => c.Id === categoriseId) || null;

      updatedRow = {
        ...row,
        Categorise: categorise
      } as IBulkCatProduct
    } else {
      const { Id, Name, Gender: genderId } = updated.Categorise; // TODO change Id to GenderId in actual data

      const valueIds = Id.split(SEPARATORS.IDS);
      const valueNames = Name.split(SEPARATORS.NAMES);

      const values = columnList.reduce((accu: any, columnKey: string, i: number) => {
        accu[columnKey] = valueIds[i] && valueNames[i] ?
          this.buildColumnValue(valueIds[i], valueNames[i], columnKey, genderId) : null;

        return accu;
      }, {});

      // TODO: do not update gender, multi select will figure out the gender
      const gender = { Id: genderId, Name: config.Gender[genderId].Name }

      updatedRow = {
        ...row,
        ...values,
        Gender: gender,
        Categorise: updated.Categorise
      };
    }

    updatedRow = this.updateCategoryByType(updatedRow);
    updatedRow = this.updateWaistbandRise(updatedRow, null);

    return this.updateStatus(updatedRow, null);
  }

  isCategorised(product: IBulkCatProduct | any) {
    return product.Status.Id === this.categorisedStatus.Id;
  }

  updateStatus(row: IBulkCatProduct, updated: any): IBulkCatProduct {
    const { FIT, STATUS } = COLUMNS;
    const { categorisedStatus, toBeProcessedStatus } = this;
    const { [FIT]: fitValue, [STATUS]: statusValue } = row; // TODO: change shape;

    if (!updated && fitValue && this.isCategorised(row)) { return row; }

    let status: IBulkCatStatus;
    if (!updated && fitValue && !this.isCategorised(row)) {
      status = categorisedStatus;
    } else if (!updated && !fitValue && !this.isCategorised(row) || !updated) {
      status = toBeProcessedStatus; // QUESTION correct status?
    } else {
      status = updated[STATUS]
    }

    return {
      ...row,
      [STATUS]: status
    };
  }

  updateStretch(row: IBulkCatProduct, updated: any, config: IBulkCatConfig, cellKey: string): IBulkCatProduct {
    const { STRETCH, STRETCHEXCEPTION } = COLUMNS;
    const updatedRow: any = this.updateDefault(row, updated, COLUMNS.STRETCH);

    const noneValue = config._StretchFactors[0]; // Todo: move to top and move all class props to separate class

    const updatedStretchException = { ...updatedRow[STRETCHEXCEPTION] };

    let shouldUpdateStretchException = true;
    for (const key in updatedStretchException) {
      if (updatedStretchException.hasOwnProperty(key) 
      && updatedStretchException[key].Id !== noneValue.Id) {
        shouldUpdateStretchException = false;
        break; 
      }
    }

    if (shouldUpdateStretchException) {
      const stretchValue = updatedRow[STRETCH];

      for (const key in updatedStretchException) {
        if (updatedStretchException.hasOwnProperty(key)) {
          updatedStretchException[key] = stretchValue;
        }
      }
    }

    return this.updateStretchException(updatedRow, {StretchException: updatedStretchException});
  }
  // TODO: Refactor all of these methods into a single method
  updateStretchException(row: IBulkCatProduct, updated: any): IBulkCatProduct {
    let updatedRow: IBulkCatProduct;
    if (!updated) {
      updatedRow = row;
    } else {
      updatedRow = {
        ...row,
        StretchException: updated.StretchException
      };
    }

    return updatedRow;
  }

  getTemplateRow(rows: IBulkCatProduct[], cellUpdate: ICellUpdate, templateRowIdx: number) {
    const rowToUpdate = rows[templateRowIdx];
    return this.updateRow(rowToUpdate, cellUpdate);
  }

  updatePropertiesFromList(oldObject: any, templateObject: any, keys: string[]) {
    const newObj = {
      ...oldObject
    };

    for (const key in templateObject) {
      if (oldObject.hasOwnProperty(key) && keys.indexOf(key) > -1) {
        // @ts-ignore (Sergio)
        newObj[key] = templateObject[key];
      }
    }

    return newObj;
  }

  // TODO: try to remove anys (Sergio) 
  updateRowByColumn(row: IBulkCatProduct | any, updated: any,
    config: IBulkCatConfig | any, configKey: string,
    columnKey: string, prevColumnKey: string, dependentColumnKey?: string): IBulkCatProduct {
    let newValue: IBulkCatColumnValue | null = null;

    // TODO: refactor to null
    if (!updated && row[prevColumnKey] && row[columnKey]) {
      const columns = config[configKey][row[prevColumnKey].Id];

      if (dependentColumnKey) {
        const name = row[columnKey].Name;
        const values = Array.isArray(columns[row[dependentColumnKey].Id]) ?
          columns[row[dependentColumnKey].Id] : [];

        newValue = this.findColumnValue(name, values) || null;

      } else if (columns && columns.length > 0) {
        newValue = this.findColumnValue(row[columnKey].Name, columns) || null;
      }

    } else {
      newValue = updated ? updated[columnKey] : null;
    }

    return {
      ...row,
      [columnKey]: newValue
    };
  }

  findColumnValue(name: string, values: IBulkCatColumnValue[]) {
    return values.find((value: IBulkCatColumnValue) => value.Name === name);
  }
}

export default RowUpdater;