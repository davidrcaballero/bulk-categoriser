import * as React from 'react';
import * as PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import List from 'material-ui/List';
import { ListItem, ListItemText } from 'material-ui/List';
import { NavLink } from 'react-router-dom';
import Divider from 'material-ui/Divider';
import SwipeableDrawer from 'material-ui/SwipeableDrawer';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';

const styles = {
  list: {
    width: 250,
    backgroundColor: 'rgba(140, 205, 195, 1)',
    button: {color: '#ffffff'}
  },
  sideList: {
    width: 'auto',
    palette: {
      secundary: {
        main:'#8dccc1'
      },
    },
  },
  drawerIcon: {
    color:'#ffffff',
    display:'none',
  },
  '@media (max-width: 2080px)': {
    drawerIcon: {
      display:'none',
    },
    list: {
     // display:'none',
    }
  },
  '@media (max-width: 1114px)': {
    drawerIcon: {
      display:'block',
      paddingBottom: 12,
      paddingTop: 12,
      backgroundColor: 'rgba(0, 0, 0, 0)',
      '&:hover': {
        backgroundColor: 'rgba(0, 0, 0, 0.12)',
        paddingBottom: 12,
        paddingTop: 12,
      }
    },
  },

  btnmenu: {
    backgroundColor: 'rgba(0, 0, 0, 0)',
    paddingTop: 17,
    paddingLeft: 10,
    paddingRight: 40,
    paddingBottom: 17,
    width: 220,
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0)',
      display: 'block',
      paddingTop: 17,
      paddingLeft: 10,
      paddingRight: 40,
      paddingBottom: 17,
      width: 220,
      color:'#83209c',
    },
  },
  active: {
    borderRight: '3px solid #83209c',
    backgroundColor: 'rgba(0, 0, 0, 0.12)',
    display: 'block'
  },
};
const theme = createMuiTheme({
  palette: {
    primary: { main: '#ffffff' }, 
  },  
  typography: {
    fontFamily: 'Pathway Gothic One',
    fontSize: '32',
  },
});

class CustomSwipeableDrawer extends React.Component<any, any> {
  constructor(props:any) {
    super(props);
    this.state = {
     left: false,
    };
  }
  static propTypes = {
    classes: PropTypes.object.isRequired,
  }

  toggleDrawer = (side:any, open:any) => () => {
    this.setState({
      [side]: open,
    });
  };

  render() {
    let iconSize = {height:40, color:'#83209c'};
    const icon = <img style={iconSize} src={require('../../assets/images/menuicon.svg')}/>
    const { classes } = this.props;
    const sideList = (
      <div className={classes.list}>
       <MuiThemeProvider theme={theme}>
        <List>
          <NavLink activeClassName={classes.active} to="/fashionqi">
            <ListItem>
              <Button className={classes.btnmenu} color="primary">FASHION IQ</Button>
            </ListItem>
          </NavLink>
          <NavLink activeClassName={classes.active} to="/fitorigin">
            <ListItem>
              <Button className={classes.btnmenu} color="primary">FIT ORIGIN</Button>
            </ListItem>
          </NavLink>
          <NavLink activeClassName={classes.active} to="/garmentmanagement">
            <ListItem>
              <Button className={classes.btnmenu} color="primary">GARMENT MANAGEMENT</Button>
            </ListItem>
          </NavLink>
          <NavLink activeClassName={classes.active} to="/supportcentre">
            <ListItem>
              <Button className={classes.btnmenu} color="primary">SUPPORT CENTRE</Button>
            </ListItem>
          </NavLink>
          <NavLink activeClassName={classes.active} to="/manageclients">
            <ListItem>
              <Button className={classes.btnmenu} color="primary">MANAGE CLIENTS</Button>
            </ListItem>
          </NavLink>
          <NavLink activeClassName={classes.active} to="/account">
            <ListItem>
              <Button className={classes.btnmenu} color="primary">ACCOUNT</Button>
            </ListItem>
          </NavLink>
        </List>
        </MuiThemeProvider>
      </div>
    );

    return (
      <div>
        <Button className={classes.drawerIcon} onClick={this.toggleDrawer('left', true)}>{icon}</Button>
        <SwipeableDrawer
          open={this.state.left}
          onClose={this.toggleDrawer('left', false)}
          onOpen={this.toggleDrawer('left', true)}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('left', false)}
            onKeyDown={this.toggleDrawer('left', false)}
          >
            {sideList}
          </div>
        </SwipeableDrawer>
      </div>
    );
  }
}

export default withStyles(styles)(CustomSwipeableDrawer);
