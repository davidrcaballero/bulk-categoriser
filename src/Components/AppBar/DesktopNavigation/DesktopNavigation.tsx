import * as React from 'react';
import * as mui from 'material-ui';
import * as PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import { NavLinkProps, NavLink, Link } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';

const styles = (theme:any) => ({
    btnmenu: {
      '&:hover': {
        paddingTop: 20,
        paddingBottom: 20,
        backgroundColor: 'rgba(0, 0, 0, 0)',
      },
    },
    flex: {
      '&:hover': {
      color:'#83209c',
      },
      fontFamily: 'Pathway Gothic One',
      fontSize: '21px',
      textDecoration:'none!important',
    },
    active: {
        backgroundColor: 'rgba(0, 0, 0, 0)',
        paddingTop: 24,
        borderBottom: '3px solid #83209c',
        paddingBottom: 23,
    },
  });
  
  const theme = createMuiTheme({
    palette: {
      primary: { main: '#83209c' },
      secondary: { main: '#ffffff' },
    },
  });

class DesktopNavigation extends React.Component<any, any> {
    constructor(props:any) {
      super(props);
      this.state = {
       value: 0,
      };
    }
    static propTypes = {
      classes: PropTypes.object.isRequired,
    }
    render() {
       const { classes } = this.props;
       return(
        <div>
            <NavLink activeClassName={classes.active} to="/fashionqi">
                <Button className={classes.btnmenu}>
                <Typography variant="title" color="secondary" className={classes.flex}>FASHION IQ</Typography>
                </Button>
            </NavLink>
            <NavLink activeClassName={classes.active} to="/fitorigin">
                <Button className={classes.btnmenu}>
                <Typography variant="title" color="secondary" className={classes.flex}>FIT ORIGIN</Typography>
                </Button>
            </NavLink>
            <NavLink activeClassName={classes.active} exact to="/">
                <Button className={classes.btnmenu}>
                <Typography variant="title" color="secondary" className={classes.flex}>GARMENT MANAGEMENT</Typography>
                </Button>
            </NavLink>
            <NavLink activeClassName={classes.active} to="/supportcentre">
                <Button className={classes.btnmenu}>
                <Typography variant="title" color="secondary" className={classes.flex}>SUPPORT CENTRE</Typography>
                </Button>
            </NavLink>
            <NavLink activeClassName={classes.active} to="/manageclients">
                <Button className={classes.btnmenu}>
                <Typography variant="title" color="secondary" className={classes.flex}>MANAGE CLIENTS</Typography>
                </Button>
            </NavLink>
            <NavLink activeClassName={classes.active} to="/account">
                <Button className={classes.btnmenu}>
                <Typography variant="title" color="secondary" className={classes.flex}>ACCOUNT</Typography>
                </Button>
            </NavLink>
        </div>
       ); 
     }
    }

export default withStyles(styles)(DesktopNavigation);
