import * as React from 'react';
import * as mui from 'material-ui';
import * as PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import { NavLink, Link } from 'react-router-dom';
import { withTheme } from 'material-ui/styles';
import CustomSwipeableDrawer from '../Drawer/CustomSwipeableDrawer';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import DesktopNavigation from './DesktopNavigation/DesktopNavigation';

const mainbarstyles = (theme:any) => ({
  root: {
    flexGrow: 1,
    marginTop: 0,
  },
  toolbar: {
    paddingLeft: 0,
    backgroundColor: 'rgba(140, 205, 195, 1)',
    minHeight: 48,
  },
  appbar: {
    backgroundColor: 'rgba(140, 205, 195, 1)',
  },
  '@media (max-width: 1114px)': {
    menuwrapper: {
      display:'none'
    }
  },
  imglogo: {
    height: '100%',
    backgroundColor:'#ffffff',
  }
  
});

const theme = createMuiTheme({
  palette: {
    primary: { main: '#83209c' },
    secondary: { main: '#ffffff' },
  },
});
class CustomAppBar extends React.Component<any, any> {
 constructor(props:any) {
   super(props);
   this.state = {
    value: 0,
   };
 }
  static propTypes = {
    classes: PropTypes.object.isRequired,
  }

  render() {
    const { classes } = this.props;
    const img = <img src={require('../../assets/images/logo.jpg')}/>
    return ( 
      <div className={classes.root}>
      <MuiThemeProvider theme={theme}>
        <AppBar className={classes.appbar} position="static">
          <Toolbar className={classes.toolbar}>
            <div className={classes.imglogo}>{img}</div>
            <div className={classes.menuwrapper}>
            <DesktopNavigation/>
            </div>
            <CustomSwipeableDrawer />
          </Toolbar>
        </AppBar>
      </MuiThemeProvider>
      </div>
    );
  }
}

export default withStyles(mainbarstyles)(CustomAppBar);
