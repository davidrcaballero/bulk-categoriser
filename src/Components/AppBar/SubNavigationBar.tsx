import * as React from 'react';
import * as mui from 'material-ui';
import * as PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import { NavLink, Link } from 'react-router-dom';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';

const styles = (theme:any) => ({
    subtoolbar: {
        backgroundColor: 'rgba(199, 199, 199, 0.51)',
        top:'0px',
        fontFamily: 'Montserrat, sans-serif',
    },
    header: {
        fontFamily: 'Pathway Gothic One',
        fontSize: '31px',
        color:'#ffffff',
        display:'inline-block',
        left:'0px'
    },
    button: {
        top: '-4px',
        left: '-15px'
    },
    rowcount: {
        paddingBottom: '10px', 
        color:'#83209c', 
        display:'none' 
    },
    tickitems: {
        display:'inline-block',
        
    },
    untickitems: {
        display:'inline-block'
    },
    totalitems: {
        display:'inline-block'
    },
    countMenu: {
        display:'inline-block',
        left: '118px',
    },
});
const theme = createMuiTheme({
    palette: {
        primary: { main: '#83209c' },
        secondary: { main: '#c7c7c7' },
    },
});

class SubNavigationBar extends React.Component<any, any> {
    constructor(props:any) {
      super(props);
      this.state = {
       value: 0,
      };
    }
    static propTypes = {
      classes: PropTypes.object.isRequired,

    } 
    
    render() {
      const { classes } = this.props;
      const IconBack = ( {fill}:any) => (
        <div style={{left:'0px', position:'relative'}}>
            <svg fill="#FFFFFF" height="30" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                <path d="M0 0h24v24H0V0z" fill="none"/>
                <path d="M11 9l1.42 1.42L8.83 14H18V4h2v12H8.83l3.59 3.58L11 21l-6-6 6-6z"/>
            </svg>
        </div>
      );
       const IconGreen = ( {fill}:any) => (
        <div style={{left:'-39px', top:'29px', position:'relative'}}>
            <svg fill={ fill } height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 0h24v24H0z" fill="none"/>
                <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"/>
            </svg>
        </div>
      );
      const IconRed = ( {fill}:any) => (
        <div style={{left:'-36px', top:'29px', position:'relative'}}>
            <svg fill={ fill } height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
                <path d="M0 0h24v24H0z" fill="none"/>
            </svg>
        </div>
      );
      const IconSave = ( {fill}:any) => (
        <div style={{left:'0px', top:'0px', position:'relative'}}>
            <svg fill="#FFFFFF" height="40" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 0h24v24H0z" fill="none"/>
                <path d="M17 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V7l-4-4zm-5 16c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm3-10H5V5h10v4z"/>
            </svg>
        </div>
      );



       return(
          <Toolbar className={classes.subtoolbar}>
            <div className={classes.menuwrapper}>
            <IconButton className={classes.button}><IconBack className={classes.backBtn} /></IconButton>
                <div style={{position:'relative', bottom:'2px'}} className={classes.header}>BULK CATEGORISATION</div>
                <div style={{position:'relative'}} className={classes.countMenu}>
                    <div style={{position:'relative', bottom:'8px', color:'#ffffff',left: '117px'}} className={classes.rowcount}>{this.props.rowText} selected: {this.props.selectedIndex}</div>
                    <div style={{position:'relative', left:'180px',bottom:'8px', color:'#ffffff'}} className={classes.tickitems}>
                    <IconGreen fill="#3f9186" /> {this.props.tickItemCount}  
                    </div>
                    <div style={{position:'relative', left:'220px',bottom:'8px', color:'#ffffff'}} className={classes.untickitems}>
                    <IconRed fill="#c24440" /> {this.props.untickItemCount}  
                    </div>
                    <div style={{position:'relative', left:'236px',bottom:'8px', color:'#ffffff'}} className={classes.totalitems}>
                    Total: {this.props.totalItemCount}  
                    </div> 
                </div>
            </div>
          </Toolbar>
       ); 
     }
    }

export default withStyles(styles)(SubNavigationBar);
