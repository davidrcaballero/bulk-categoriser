import * as React from 'react';
var sectionStyle = {width: "100%",height: "auto"};
 export default class Account extends React.Component<any, any> {
     
    render() {
        const img = <img style={ sectionStyle } src={require('../../assets/images/page1.png')}/>
        return (
        <div>
          <h1>Account Page</h1>
          <section>{img}</section>
        </div>)
    }
}
