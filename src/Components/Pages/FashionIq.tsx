import * as React from 'react';
var sectionStyle = {width: "100%",height: "auto"};
 export default class FashionIq extends React.Component<any, any> {
    render() {
        const img = <img style={ sectionStyle } src={require('../../assets/images/page2.png')}/>
        return (
        <div>
          <h1>Fashion IQ Page</h1>
          <section>{img}</section>
        </div>)
    }
}
