import * as React from 'react';
var sectionStyle = {width: "100%",height: "auto"};
 export default class ManageClients extends React.Component<any, any> {
    render() {
        const img = <img style={ sectionStyle } src={require('../../assets/images/page4.jpg')}/>
        return (
        <div>
          <h1>Manage Clients Page</h1>
          <section>{img}</section>
        </div>)
    }
}
