import * as React from 'react';
var sectionStyle = {width: "100%",height: "auto"};
 export default class FitOrigin extends React.Component<any, any> {
    render() {
        const img = <img style={ sectionStyle } src={require('../../assets/images/page3.jpg')}/>
        return (
        <div>
          <h1>Fit Origin Page</h1>
          <section>{img}</section>
        </div>)
    }
}
