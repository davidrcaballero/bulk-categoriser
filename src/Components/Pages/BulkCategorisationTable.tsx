import * as React from 'react';
import * as ReactDataGrid from 'react-data-grid';
import { Editors, Toolbar, Formatters } from 'react-data-grid-addons';
import * as PropTypes from 'prop-types';
import { ITableRow } from '../../interfaces/ITableRow';
import { Column } from 'react-data-grid';
import { ICategoriesCombined } from '../../interfaces/IBulkCatConfig';

import SelectEditor from '../BulkCategorisationTable/editors/SelectEditor';
import SelectAutoCompleteEditor from '../BulkCategorisationTable/editors/SelectAutoCompleteEditor';
import MultiSelectEditor from '../BulkCategorisationTable/editors/MultiSelectEditor';

import SelectFormatter from '../BulkCategorisationTable/formatters/SelectFormatter';
import ImageFormatter from '../BulkCategorisationTable/formatters/ImageFormatter';

import { withRouter } from 'react-router';
const update = require('immutability-helper');
const bulkCatConfig = require('../../localData/bulkCatConfig.json'); // TODO replace with fetch
const { AutoComplete: AutoCompleteEditor, SimpleTextEditor } = Editors;
import GarmentInfoText from '../BulkCategorisationTable/formatters/GarmentInfoText';
import CustomHeader from '../BulkCategorisationTable/formatters/CustomHeader';
import IconStatus from '../BulkCategorisationTable/formatters/IconStatus';
import SubNavigationBar from '../AppBar/SubNavigationBar';
import Button from 'material-ui/Button';

interface GridRowsUpdatedEvent extends ReactDataGrid.GridRowsUpdatedEvent {
  updated: {
    event?: React.KeyboardEvent<Event>
    focusedValue?: any | undefined
  };
}

/**
 * `connect` is the second thing to import from redux.
 * The connect function returns a function which wraps Components to connect with the Redux store
 */

import { connect } from 'react-redux';
import * as actions from '../../store/actions'; // make sure you understand that index is found by default :-)
import RowUpdater from '../../services/RowUpdater';
import { IBulkCatProduct } from '../../interfaces/IBulkCatProduct';
import InputEditor from '../BulkCategorisationTable/editors/InputEditor';
import MultiSelectFormatter from '../BulkCategorisationTable/formatters/MultiSelectFormatter';
import { COLUMNS } from '../../constants';


// TODO: remove all ts-ignores

// QUESTION: badly needs an undo feature? With name of column name and/ content change
class BulkCategorisationTable extends React.Component<any, any> {
  state: any;
  private columns: any[]; // should be type Column
  private tableDimensions: any; // should be Column
  private grid: any;
  private categoriesCombined: ICategoriesCombined[];
  private categorise: any;
  private categoriesCombinedByGender: any;
  private rowUpdater: RowUpdater;

  constructor(props: any) {
    super(props);

    let { config, products } = this.props;
    console.log({ config }, { products });

    const originalRows = this.props.products;
    const rows = originalRows.slice(0);
    this.rowUpdater = new RowUpdater(config);

    this.state = {
      rows,
      optionKey: null,
      selectedIndexes: [],
      selectedInputId: '',
      originalRows,
      selectedColumnCoordinates: null,
      dataSaved: false,
      check: false,
      processed: false,
      rowIndex: 0,
      count: 0,
    };

    // TODO: these need to be updated on browser resize
    this.columns = this.getColumns(); 
    this.tableDimensions =  this.getTableDimensions();
  }

  getColumns() {
    console.log('------- Updated Columns ------- (Bad performance)')
    const { config } = this.props;

    const categoriesCombinedByGender =
      this.transformToCategoriesCombinedByGender(config.CategoriesCombined);

    const stretchExceptionsOrderKeys = ['Bust', 'Waist', 'Hips']; // Should be const
    const stretchExceptionsLabels = ["B", "W", "H"];

    const columnWidth = window.innerWidth / 10;

    const { QA, GARMENT_INFO, CATEGORISE, IMAGE, MEASUREMENTS, GENDER, TYPE,
      SUBTYPE, CATEGORY, CUT, FIT, WAISTBANDRISE, STRETCH, STRETCHEXCEPTION, STATUS } = COLUMNS;

    // debugger
    const windowWidth = window.innerWidth - 40;
    const baseWidth = 1920 - 40;

    const calcuateWidth = (width: number) => {
      const widthPercent = width / baseWidth;
      return Math.ceil(widthPercent * windowWidth);
    }

    return [
      {
        key: QA,
        name: 'QA',
        editable: false,
        width: calcuateWidth(50),
        resizable: true,
        sortable: true,
        formatter: <IconStatus />,
        getRowMetaData: (rowdata: any) => rowdata,
      },
      {
        key: GARMENT_INFO,
        name: 'Garment info',
        editable: false,
        width: calcuateWidth(220),
        resizable: true,
        sortable: true,
        headerRenderer: <CustomHeader handleGridSort={this.handleGridSort} />,
        formatter: <GarmentInfoText />,
        getRowMetaData: (rowdata: any) => rowdata,
      },
      {
        key: IMAGE,
        name: 'Image',
        width: calcuateWidth(120),
        sortable: true,
        formatter: <ImageFormatter width={calcuateWidth(110)} height={85} />,
      },
      {
        key: MEASUREMENTS,
        name: 'Measurements',
        width: calcuateWidth(120),
        sortable: true,
        resizable: true,
        editable: false,
        formatter: <SelectFormatter valueKey="Id" textKey="Name" />
      },
      {
        key: GENDER,
        name: 'Gender',
        width: calcuateWidth(90),
        sortable: true,
        resizable: true,
        editor: <SelectAutoCompleteEditor
          grid={() => this.grid}
          nullable={true}
          initialShowAll={true}
          valueKey="Id"
          textKey="Name"
          resultsWidth={90}
          filterable={true}
          options={config.Gender} />, // TODO: consistent naming - Gender should be Genders
        formatter: <SelectFormatter valueKey="Id" textKey="Name" />
      },
      {
        key: CATEGORY,
        name: 'Category',
        width: calcuateWidth(90),
        sortable: true,
        resizable: true,
        editor: <SelectAutoCompleteEditor
          grid={() => this.grid}
          initialShowAll={true}
          valueKey="Id"
          textKey="Name"
          resultsWidth={calcuateWidth(90)}
          getOptions={(options: any, rowMetaData: any) => this.getOptions(options, rowMetaData, "Gender")}
          options={config._Categories} />, // TODO: consistent naming - Gender should be Genders
        formatter: <SelectFormatter valueKey="Id" textKey="Name" />,
        getRowMetaData: (rowdata: any) => rowdata
      },
      {
        key: TYPE,
        name: 'Garment Type',
        width: calcuateWidth(110),
        sortable: true,
        resizable: true,
        editor: <SelectAutoCompleteEditor
          grid={() => this.grid}
          valueKey="Id"
          textKey="Name"
          nullable={true}
          initialShowAll={true}
          resultsWidth={100}
          getOptions={(options: any, rowMetaData: any) => this.getOptions(options, rowMetaData, "Gender", "Category")}
          options={config._TypesByCategory} />,
        formatter: <SelectFormatter valueKey="Id" textKey="Name" />,
        getRowMetaData: (rowdata: any) => rowdata
      },
      {
        key: SUBTYPE,
        name: 'Subtype',
        width: calcuateWidth(100),
        sortable: true,
        resizable: true,
        editor: <SelectAutoCompleteEditor
          grid={() => this.grid}
          valueKey="Id"
          textKey="Name"
          nullable={true}
          initialShowAll={true}
          resultsWidth={120}
          getOptions={(options: any, rowMetaData: any) => this.getOptions(options, rowMetaData, "Type")}
          options={config.SubTypes} />,
        formatter: <SelectFormatter valueKey="Id" textKey="Name" />,
        getRowMetaData: (rowdata: any) => rowdata
      },
      {
        key: CUT,
        name: 'Cut',
        width: calcuateWidth(100),
        sortable: true,
        resizable: true,
        editor: <SelectAutoCompleteEditor
          grid={() => this.grid}
          valueKey="Id"
          textKey="Name"
          nullable={true}
          initialShowAll={true}
          resultsWidth={100}
          getOptions={(options: any, rowMetaData: any) => this.getOptions(options, rowMetaData, "Subtype")}
          options={config.Cut} />, // should be cuts
        formatter: <SelectFormatter valueKey="Id" textKey="Name" />,
        getRowMetaData: (rowdata: any) => rowdata
      },
      {
        key: FIT,
        name: 'Fit',
        width: calcuateWidth(90),
        sortable: true,
        resizable: true,
        editor: <SelectAutoCompleteEditor
          grid={() => this.grid}
          valueKey="Id"
          textKey="Name"
          nullable={true}
          initialShowAll={true}
          resultsWidth={100}
          getOptions={(options: any, rowMetaData: any) => this.getOptions(options, rowMetaData, "Cut")}
          options={config.Fit} />, // should be fits
        formatter: <SelectFormatter valueKey="Id" textKey="Name" />,
        getRowMetaData: (rowdata: any) => rowdata
      },
      {
        key: CATEGORISE,
        name: 'Categorise',
        width: calcuateWidth(225),
        sortable: true,
        resizable: true,
        editor: <SelectAutoCompleteEditor
          grid={() => this.grid}
          valueKey="Id"
          textKey="Name"
          resultsWidth={455}
          getOptions={this.getCategoriesCombinedOptions}
          options={categoriesCombinedByGender} />,
        formatter: <SelectFormatter valueKey="Id" textKey="Name" />,
        getRowMetaData: (rowdata: any) => rowdata,

      },
      {
        key: WAISTBANDRISE,
        name: 'Waistband Rise',
        width: calcuateWidth(90),
        sortable: true,
        resizable: true,
        editable: this.isWaistbandRiseEditable,
        editor: <SelectAutoCompleteEditor
          grid={() => this.grid}
          valueKey="Id"
          textKey="Name"
          initialShowAll={true}
          nullable={true}
          resultsWidth={100}
          options={config._WaistbandRises} />,
        formatter: <SelectFormatter valueKey="Id" textKey="Name" />,
      },
      {
        key: STRETCH,
        name: 'Stretch',
        width: calcuateWidth(90),
        sortable: true,
        resizable: true,
        editor: <SelectAutoCompleteEditor
          grid={() => this.grid}
          valueKey="Id"
          textKey="Label"
          initialShowAll={true}
          nullable={true}
          resultsWidth={100}
          options={config._StretchFactors} />,
        formatter: <SelectFormatter valueKey="Id" textKey="Label" />,
      },
      {
        key: STRETCHEXCEPTION,
        name: 'Stretch Exception',
        width: calcuateWidth(140),
        sortable: true,
        resizable: true,
        editor: <MultiSelectEditor
          grid={() => this.grid}
          height={4}
          valueKey="Id"
          textKey="Label"
          labels={stretchExceptionsOrderKeys}
          initialShowAll={true}
          nullable={true}
          inputWidth={60}
          resultsWidth={100}
          inputOrderKeys={stretchExceptionsOrderKeys}
          getSelectedInputId={() => this.state.selectedInputId}
          options={config._StretchExceptions} />,
        formatter: <MultiSelectFormatter
          valueKey="Id"
          textKey="Label"
          inputWidth={60}
          labels={stretchExceptionsOrderKeys}
          inputOrderKeys={stretchExceptionsOrderKeys}
        />,
        // events: {
        //   onClick: this.handleMultiSelect
        // },
      },
      {
        key: STATUS,
        name: 'Status',
        width: calcuateWidth(110),
        sortable: true,
        resizable: true,
        editor: <SelectAutoCompleteEditor
          grid={() => this.grid}
          valueKey="Id"
          textKey="Name"
          initialShowAll={true}
          resultsWidth={100}
          getOptions={this.getCategoriesCombinedOptions}
          options={config._Statuses} />,
        formatter: <SelectFormatter valueKey="Id" textKey="Name" />,
      }
    ];
  }

  getTableDimensions() {
    const navigationHeight = 66;
    const bulkHeight = 66;
    const margin = 40;

    return { height: window.innerHeight - navigationHeight - bulkHeight - margin }
  }

  // TODO: improve or remove
  // handleMultiSelect = (e: any, column: any) => {
  //   const { rowIdx, idx } = column;
  //   const inputIndex = e.target.dataset && e.target.dataset.inputIndex;

  //   this.setState(() => ({
  //     selectedInputId: inputIndex,
  //     selectedColumnCoordinates: { rowIdx, idx }
  //   }), () => this.grid.openCellEditor(rowIdx, idx));
  // }

  // TODO: move to different class
  getCategoriesCombinedOptions = (options: any, rowMetaData: any) => {
    return rowMetaData.Gender ? options[rowMetaData.Gender.Id] : options.All;
  }

  getOptions = (options: any, rowMetaData: any, columnKey: string, dependentColumnKey?: string) => {
    const { [columnKey]: selectedValue } = rowMetaData;

    if (!selectedValue) { return []; }

    let optionsByColumn = options[selectedValue.Id];

    if (dependentColumnKey) {
      optionsByColumn = optionsByColumn[rowMetaData[dependentColumnKey].Id];
    }

    return optionsByColumn || [];
  }

  getTypeOptions = (options: any, rowMetaData: any) => {


    const { Gender, Category } = rowMetaData;
    return options[Gender.Id][Category.Id];
  }

  // QUESTION: should this list be filtered by gender or also control the gender the column?
  // If show all, it'll need to display the gender beside the categorise item 
  transformToCategoriesCombinedByGender(categoriesCombined: Array<ICategoriesCombined>) {
    const categoriesCombinedByGender: any = categoriesCombined
      .reduce((categoriesCombinedByGender: any, catCombined: ICategoriesCombined) => {

        !categoriesCombinedByGender[catCombined.Gender]
          ? categoriesCombinedByGender[catCombined.Gender] = [catCombined]
          : categoriesCombinedByGender[catCombined.Gender] = [...categoriesCombinedByGender[catCombined.Gender], catCombined]

        return categoriesCombinedByGender;
      }, {});

    categoriesCombinedByGender.All = categoriesCombined;

    return categoriesCombinedByGender;
  }

  combinedGenderOptions = () => {
    return this.transformToCategoriesCombinedByGender(this.props.config.CategoriesCombined);
  }

  componentDidMount() {
    //console.log('this.props:::componentDidMount ',this.props);
    //console.log('localStorage ',localStorage.getItem("route")); 
    if (this.state.dataSaved) {
      this.props.history.push('/account');
    }

  }
  // shouldComponentUpdate(nextProps: any, nextState: any) {
  //   //console.log(nextProps, nextState);
  //   //console.log(this.props, this.state);

  //   return true;
  // }
  componentWillReceiveProps(nextProps: any, nextState: any) {
    // console.log('nextProps.history ',nextProps.history);
    //  console.log('nextProps ',nextProps);
    //  console.log('nextState ',nextState);
    // nextProps.history.listen((location:any) =>{console.log('location::: ',location)} );
    // this.props.routeLocationDidUpdate(location)
  }

  componentDidUpdate(prevProps: any, prevState: any) {

  }
  componentWillUnmount() {
    // console.log('the index componentWillUnmount',this.state.rowIndex);
    //this.props.history.listen((location:any) => console.log(location.pathname));
    //console.log('this.props.history::: ',this.props.history);
    //this.props.routeLocationDidUpdate(location.pathname);
    console.log('this.props::: ', this.props.history.location.pathname);
    localStorage.setItem('route', this.props.history.location.pathname);
    //this.props.history.push('/');
    if (!this.state.dataSaved) {
      setTimeout(() => {
        // console.log('rout after tiemout ',localStorage.getItem("routeLocation"));
        // this.props.history.replace(localStorage.getItem("routeLocation"));

      }, 10);
    }


    // if(localStorage.getItem("route") !== '/'){

    //   setTimeout(() => {
    //     //this.props.history.push(localStorage.getItem("route"));
    //     localStorage.setItem('route', '/');
    //    }, 2000);
    // }

  }

  rowGetter = (i: number) => {
    return this.state.rows[i];
  }

  handleRowsSelected = (rows: any) => {
    this.setState({ selectedIndexes: this.state.selectedIndexes.concat(rows.map((r: any) => r.rowIdx)) });
  }

  handleRowsDeselected = (rows: any) => {
    let rowIndexes = rows.map((r: any) => r.rowIdx);
    this.setState({ selectedIndexes: this.state.selectedIndexes.filter((i: number) => rowIndexes.indexOf(i) === -1) });
  }

  handleGridFilter = (data: any) => {

  }
  // PoC sort
  handleGridSort = (sortColumn: any, sortDirection: any) => {
    console.log('the element to sort ', sortColumn);
    const columnValueNameKeys: any = {
      Category: "Name",
      Gender: "Name",
      Subtype: "Name",
      Cut: "Name",
      Fit: "Name",
      Categorise: "Name",
    }

    if (sortColumn === 'ArticleNumber' || sortColumn === 'Title') {
      var toggleSort = this.toggleSorting(this.state.check, sortColumn);
      const rows = this.state.rows.sort(toggleSort());
      this.setState({ rows, check: !this.state.check });
    } else {
      const comparer = (a: any, b: any) => {
        if (sortDirection === 'ASC') {
          if (columnValueNameKeys[sortColumn]) {
            const name = columnValueNameKeys[sortColumn];
            return (a[sortColumn][name] > b[sortColumn][name]) ? 1 : -1;
          } else {
            return (a[sortColumn] > b[sortColumn]) ? 1 : -1;
          }
        } else if (sortDirection === 'DESC') {
          if (columnValueNameKeys[sortColumn]) {
            const name = columnValueNameKeys[sortColumn];
            return (a[sortColumn][name] < b[sortColumn][name]) ? 1 : -1;
          } else {
            //console.log('or here DESC',a[sortColumn]);
            return (a[sortColumn] < b[sortColumn]) ? 1 : -1;
          }
        }
      };
      const rows = sortDirection === 'NONE' ? this.state.originalRows.slice(0) : this.state.rows.sort(comparer);
      this.setState({ rows, check: !this });
    }
  }
  toggleSorting = (reversed: any, sortColumn: any) => {
    return function () {
      reversed = !reversed;
      return function (a: any, b: any) {
        return (a[sortColumn] == b[sortColumn] ? 0 : a[sortColumn] < b[sortColumn] ? -1 : 1) * (reversed ? -1 : 1);
      };
    };
  };

  handleCellSelected = ({ rowIdx, idx }: any) => {
    this.setState((prevState: any) => {
      return prevState.selectedColumnCoordinates = { rowIdx, idx };
    }, () => {
      // this.grid.openCellEditor(rowIdx, idx)
    });
    this.setState({ rowIndex: rowIdx });
  }

  handleGridRowsUpdated = (gridRowsUpdatedEvent: GridRowsUpdatedEvent) => {
    const { fromRow, toRow, updated, action, cellKey } = gridRowsUpdatedEvent;
    let keyPressed = '';

    if (updated.event && updated.event.key) { keyPressed = updated.event.key; }

    if (keyPressed === "Escape") { this.grid.deselect(); }

    if (!keyPressed && updated.focusedValue === null) { return; };

    console.log('|--------- Grid Rows Updated  ------------|');

    // @ts-ignore
    console.log(`Cell ${cellKey}: `, updated && updated[cellKey]);

    const { selectedIndexes, selectedColumnCoordinates: coord } = this.state;
    const { rowUpdater } = this;

    // QA count not working
    // if(updated.focusedValue.Name === "Categorized") this.setState({ count: this.state.count + 1});
    // this.setState((prevState:any) => {
    //   if (this.state.rows[rowIdx].Status.Name === "Categorized")  return { count: this.state.count - 1 }
    // });

    let rows = this.state.rows.slice();
    const rowIdx = this.state.selectedColumnCoordinates.rowIdx;

    const cellUpdate = {
      ...gridRowsUpdatedEvent,
      config: this.props.config,
      rows
    };

    if (rowUpdater.isRowFromSelectedIndexes(selectedIndexes, rowIdx)) {
      const cloneSelectedIndexes = selectedIndexes.slice();
      rows = rowUpdater.updateRowsBySelectedIndexes(cellUpdate, cloneSelectedIndexes, rowIdx, cellKey);

    } else if (selectedIndexes.length > 0 && coord.rowIdx >= 0) { // needs to be abstracted to function
      const rowToUpdate = rows[rowIdx];
      rows[rowIdx] = rowUpdater.updateRow(rowToUpdate, cellUpdate);

    } else if (fromRow === toRow && !rowUpdater.isRowFromSelectedIndexes(selectedIndexes, rowIdx)) {
      const rowToUpdate = rows[rowIdx];
      const updatedRow = rowUpdater.updateRow(rowToUpdate, cellUpdate);

      rows[rowIdx] = updatedRow;

    } else if (fromRow !== toRow) {
      rows = rowUpdater.updateRowsFromTo(rows, cellUpdate, fromRow, toRow, selectedIndexes, rowIdx);
    }

    console.log(`Row ${rowIdx}: `, rows[rowIdx]);
    // console.log(`JSON ${rowIdx}: `, JSON.stringify(rows[rowIdx]));
    // console.log(`JSON All Rows: `, JSON.stringify(rows));

    this.setState((prevState: any) => prevState.rows = rows,
      () => {
        if (keyPressed === "Enter") { this.grid.moveSelectedCell(updated.event, 1, 0); }
        else if (keyPressed === "Tab") { this.grid.moveSelectedCell(updated.event, 0, 1); }
      });
  };

  isWaistbandRiseEditable = (rowData: IBulkCatProduct | any) => { // TODO: remove any
    const categoryUpperId = 0;
    return rowData.Category && rowData.Category.Id !== categoryUpperId;
  }

  render() {
    // Redux flow: 4th and last thing to be called props should now be updated
    const rowText = this.state.selectedIndexes.length === 1 ? 'row' : 'rows';
    const untick = this.state.rows.length - this.state.count;

    const reactDataGridStyle: any = { margin: 20 };

    return (
      <div>
        {/* <div>
          Sort Category example -
          <button onClick={() => this.handleGridSort("Gender", "ASC")}>Sort by ASC</button>
          <button onClick={() => this.handleGridSort("Gender", "DESC")}>Sort by DESC</button>
          <button onClick={() => this.handleGridSort("Gender", "NONE")}>Sort by None</button>
        </div>
        <span>{this.state.selectedIndexes.length} {rowText} selected</span> */}
        <SubNavigationBar
          selectedIndex={this.state.selectedIndexes.length}
          rowText={rowText} tickItemCount={this.state.count} untickItemCount={untick} totalItemCount={this.state.rows.length}
        />
        <div style={reactDataGridStyle}>
          <ReactDataGrid
            rowKey="id"
            ref={node => this.grid = node}
            columns={this.columns}
            rowGetter={this.rowGetter}
            rowsCount={this.state.rows.length}
            rowHeight={100}
            headerRowHeight={50}
            enableCellSelect={true}
            onGridRowsUpdated={this.handleGridRowsUpdated}
            onCellSelected={(coordinates: any) => { this.handleCellSelected(coordinates) }}
            // @ts-ignore

            // rowActionsCell={RowActionsCell}

            // onCellDeSelected={(e: any)=> { }}
            // onRowClick={(e: any)=> {console.log({onRowClick: e})}}
            // onAddFilter={this.handleGridFilter}
            onGridSort={this.handleGridSort}

            // enableRowSelect="multi"
            minHeight={this.tableDimensions.height}
            rowSelection={{ // consider implementing our own checkbox to make the column more power (click and drag to select multiple rows); 
              showCheckbox: true, // known Warning: Failed prop type: You provided a `checked` https://github.com/adazzle/react-data-grid/issues/949
              enableShiftSelect: true,
              onRowsSelected: this.handleRowsSelected,
              onRowsDeselected: this.handleRowsDeselected,
              // getCellActions={(e: any)=> {console.log({getCellActions: e})}},

              selectBy: {
                indexes: this.state.selectedIndexes
              }
            }}
          />
        </div>
      </div>);
  }
}

// BulkCategorisationTable.propTypes = {
//   config: PropTypes.object.isRequired,
//   products: PropTypes.array.isRequired,
// };

/**
 * Optional
 * `mapStateToProps` declare and pass only the data the component needs from the store (under the hood optimisations)
 * Transform and sort state here for components to use
 *
 * @param {state} Object is the state returned from a reducer
 * @param {ownProps} Object reference to the components own props using `this.props`. `ownProps` is mainly used by React Router
 */
const mapStateToProps = (state: any) => {
  // Redux flow: 3rd thing to be called

  return {
    config: state.data.config, // `.data` determined by property name used in rootReducer TODO: rename
    products: state.data.products,
    location: state.location
  };
}

/**
 * Optional
 * `mapDispatchToProps` used to decide which actions we'd like to expose on the component.
 * The component will receive a `this.props.dispatch` propoperty when this function is not passed to connect.
 * 
 * Dispatch is a function that allows us to fire off actions defined in `actions` (/../store/actions)
 *
 * @param {state} Object is the state returned from a reducer
 * @param {ownProps} Object reference to the components own props using `this.props`. `ownProps` is mainly used by React Router
 */
const mapDispatchToProps = (dispatch: any) => {
  return {
    getConfig: () => dispatch(actions.getConfig()),
    getProducts: () => dispatch(actions.getProducts()),
    routeLocationDidUpdate: (location: any) => dispatch(actions.routeLocationDidUpdate(location)),
    //setGarmentStatus: (garmentStatus: any) => dispatch(actions.setGarmentStatus(garmentStatus)),
    //onRowsUpdate: (updated:any) => dispatch(actions.sendRowsUpdate(updated)),
  }
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(BulkCategorisationTable) as any
);


// TODO: should be removed with server supplying correct data shape
function transformToCategoriseKeyValuePair(products: any, config: any) {
  return products.map((product: any) => {
    const categorise = config.CategoriesCombined
      .find((c: any) => c.Id === product.Categorise);

    return {
      ...product,
      Categorise: categorise
    };
  });
}

function transformToGenderKeyValuePair(products: any, config: any) {
  return products.map((product: any) => {
    const gender = config.Gender
      .find((g: any) => g.Id === product.Gender);

    return {
      ...product,
      Gender: gender
    };
  });
}

function transformToCategoryKeyValuePair(products: any, config: any) {
  return products.map((product: any) => {
    return {
      ...product, // spread object
      Category: { Id: product.CategoryType, Name: product.CategoryTypeString }
    };
  });
}

// function transformToStatusKeyValuePair(products: any, config: any) {
//   return products.map((product: any) => {
//     return {
//       ...product, // spread object
//       Status: { Id: product.StateId, Name: product.StateName }
//     };
//   });
// }

// function renameStatusTitleToName(list: [any]) {
//   return list.map((listItem: any) => {
//     return {
//       Id: listItem.Id,
//       Name: listItem.Title
//     }
//   });
// }
