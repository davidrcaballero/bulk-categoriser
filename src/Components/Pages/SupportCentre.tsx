import * as React from 'react';
var sectionStyle = {width: "100%",height: "auto"};
 export default class SupportCentre extends React.Component<any, any> {
    render() {
        const img = <img style={ sectionStyle } src={require('../../assets/images/page5.jpg')}/>
        return (
        <div>
          <h1>Support Centre Page</h1>
          <section>{img}</section>
        </div>)
    }
}
