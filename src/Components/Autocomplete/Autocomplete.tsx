import * as PropTypes from 'prop-types';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
// import * as classnames from 'classnames';

class Autocomplete extends React.Component<any, any> {
  static displayName = 'Autocomplete';
  blurTimer: NodeJS.Timer | null;
  refs: any;


  static propTypes = {
    options: PropTypes.any,
    search: PropTypes.func,
    resultRenderer: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.func
    ]),
    value: PropTypes.object,
    onChange: PropTypes.func,
    onError: PropTypes.func,
    onFocus: PropTypes.func
  };

  static defaultProps = { search: searchArray, style: {} };

  mounted: boolean;
  constructor(props: any) {
    super(props);

    const searchTerm = props.searchTerm ?
      props.searchTerm :
      props.value ?
        props.value[props.textKey] :
        '';

    this.state = {
      results: [],
      event: {},
      showResults: false,
      showResultsInProgress: false,
      searchTerm: searchTerm,
      focusedValue: null,
      keyPressed: ''
    };
  }

  getResultIdentifier = (result: any) => {
    if (this.props.resultIdentifier === undefined) {
      return result.id;
    } else {
      return result[this.props.resultIdentifier];
    }
  };

  componentWillReceiveProps(nextProps: any) {
    var searchTerm = nextProps.searchTerm ?
      nextProps.searchTerm :
      nextProps.value ?
        nextProps.value[nextProps.textKey] :
        '';

    // this.setState({ searchTerm: searchTerm });
  }

  componentWillMount() {
    this.blurTimer = null;
  }

  componentDidMount() {
    // this.mounted = true;
  }

  componentWillUnmount() {
    // this.mounted = fal.se; 
  }

  /**
    * Show results for a search term value.
    *
    * This method doesn't update search term value itself.
    *
    * @param {Search} searchTerm
    */
  showResults = (searchTerm: any) => {
    this.setState({ showResultsInProgress: true });
    this.props.search(
      this.props.options,
      searchTerm.trim(),
      this.onSearchComplete
    );
  };

  showAllResults = () => {
    if (!this.state.showResultsInProgress && !this.state.showResults) {
      this.showResults('');
    }
  };

  handleValueChange = (e: React.KeyboardEvent<HTMLElement> | React.MouseEvent<HTMLElement>, result: any, ) => {
    const { textKey, onChange } = this.props;

    // React reuses events. We need to pass it async through setState so we call persist
    // https://reactjs.org/docs/events.html#event-pooling
    e.persist();

    const state = {
      value: result,
      showResults: false,
      searchTerm: this.state.searchTerm,
      event: e || {}
    };

    if (result) { state.searchTerm = result[textKey]; }

    this.setState((prevState: any) => state,
      () => {
        if (onChange) {
          onChange(result);
        }
      });
  };

  onSearchComplete = (err: any, results: any) => {
    if (err) {
      if (this.props.onError) {
        this.props.onError(err);
      } else {
        throw err;
      }
    }

    this.setState({
      showResultsInProgress: false,
      showResults: true,
      results: results
    });
  };

  handleMouseEnterResultList = (value: any) => {
    this.setState({ focusedValue: value });
  };

  handleMouseLeaveResultList = (value: any) => {
    this.setState({ focusedValue: null });
  };

  handleQueryChange = (e: any) => {
    const searchTerm = e.target.value;

    this.setState({
      searchTerm: searchTerm,
      focusedValue: null
    });

    this.showResults(searchTerm);
  };

  onFocus = () => {
    if (this.blurTimer) {
      clearTimeout(this.blurTimer);
      this.blurTimer = null;
    }
    this.refs.search.focus();
  };

  onSearchInputFocus = () => {
    if (this.props.onFocus) {
      this.props.onFocus();
    }

    this.showAllResults();
  };

  onBlur = () => {

    // if(!this.mounted) {
    // //   this.blurTimer = setTimeout(() => {
    // // //     this.setState({ showResults: false });
    // //   }, 100);
    // }
    // wrap in setTimeout so we can catch a click on results
    // this.blurTimer = setTimeout(() => {
    //   this.setState({ showResults: false });
    // }, 100);
  };

  onQueryKeyDown = (e: any) => {
    const { focusedValue } = this.state;
    const { grid } = this.props;
    const keyPressed = e.key;

    if (keyPressed === 'Escape') {
      e.preventDefault();
      e.stopPropagation();

      this.handleValueChange(e, focusedValue);

    }
    else if (keyPressed === 'Enter') {
      e.preventDefault();
      e.stopPropagation();

      this.handleValueChange(e, focusedValue);

    } else if (keyPressed === 'Tab') {
      e.preventDefault();
      e.stopPropagation();

      this.handleValueChange(e, focusedValue);

    } else if (keyPressed === 'ArrowUp' && this.state.showResults) {
      e.preventDefault();
      var prevIdx = Math.max(
        this.focusedValueIndex() - 1,
        0
      );
      this.setState({
        focusedValue: this.state.results[prevIdx]
      });

    } else if (keyPressed === 'ArrowDown') {
      e.preventDefault();
      if (this.state.showResults) {
        var nextIdx = Math.min(
          this.focusedValueIndex() + (this.state.showResults ? 1 : 0),
          this.state.results.length - 1
        );
        this.setState({
          showResults: true,
          focusedValue: this.state.results[nextIdx]
        });
      } else {
        this.showAllResults();
      }
    }
  };

  focusedValueIndex = () => {
    if (!this.state.focusedValue) {
      return -1;
    }
    for (var i = 0, len = this.state.results.length; i < len; i++) {
      if (this.getResultIdentifier(this.state.results[i]) === this.getResultIdentifier(this.state.focusedValue)) {
        return i;
      }
    }
    return -1;
  };

  render() {
    const { results, focusedValue, showResults, searchTerm } = this.state;
    const { textKey, valueKey, width, resultsWidth, style } = this.props;

    const divStyle: any = {
      position: 'relative',
      outline: 'none'
    };

    const classResultsShown = this.state.showResults ? 'react-autocomplete-Autocomplete--resultsShown' : '';

    return (
      <div
        // tabIndex="1"
        className={'react-autocomplete-Autocomplete ' + classResultsShown}
        // className={className}
        onFocus={this.onFocus}
        onBlur={this.onBlur}
        style={divStyle}
      >
        <input
          ref="search"
          className="react-autocomplete-Autocomplete__search"
          style={style}
          onClick={this.showAllResults}
          onChange={this.handleQueryChange}
          onFocus={this.onSearchInputFocus}
          // onBlur={this.onQueryBlur}
          onKeyDown={this.onQueryKeyDown}
          value={this.state.searchTerm}
        />
        <ResultList
          className="react-autocomplete-Autocomplete__results"
          onMouseEnterResultList={this.handleMouseEnterResultList}
          onMouseLeaveResultList={this.handleMouseLeaveResultList}
          onClickResultItem={this.handleValueChange}
          results={this.state.results}
          focusedValue={this.state.focusedValue}
          show={this.state.showResults}
          width={resultsWidth}
          renderer={this.props.resultRenderer}
          label={this.props.label} // TODO: change name to textKey (DC)
          resultIdentifier={this.props.resultIdentifier} //TODO: change name to valueKey (DC)
          valueKey={valueKey}
        />
      </div>
    );
  }
}

class ResultList extends React.Component<any, any> {
  ignoreFocus: boolean;

  getResultIdentifier = (result: any) => {

    if (this.props.resultIdentifier === undefined) {
      if (!result.id) {
        throw Error("id property not found on result. You must specify a resultIdentifier and pass as props to autocomplete component");
      }
      return result.id;
    } else {
      return result[this.props.resultIdentifier];
    }
  };

  componentDidUpdate() {
    this.scrollToFocused();
  }

  componentDidMount() {
    this.scrollToFocused();
  }

  componentWillMount() {
    this.ignoreFocus = false;
  }

  scrollToFocused = () => {
    var focused = this.refs && this.refs.focused;
    if (focused) {
      var containerNode: any = ReactDOM.findDOMNode(this);
      var scroll = containerNode.scrollTop;
      var height = containerNode.offsetHeight;

      var node: any = ReactDOM.findDOMNode(focused);
      var top = node.offsetTop;
      var bottom = top + node.offsetHeight;

      // we update ignoreFocus to true if we change the scroll position so
      // the mouseover event triggered because of that won't have an
      // effect
      if (top < scroll) {
        this.ignoreFocus = true;
        containerNode.scrollTop = top;
      } else if (bottom - scroll > height) {
        this.ignoreFocus = true;
        containerNode.scrollTop = bottom - height;
      }
    }
  };

  handleMouseEnterResultItem = (e: any, result: any) => {
    // check if we need to prevent the next onFocus event because it was
    // probably caused by a mouseover due to scroll position change
    if (this.ignoreFocus) {
      this.ignoreFocus = false;
    } else {
      // we need to make sure focused node is visible
      // for some reason mouse events fire on visible nodes due to
      // box-shadow
      var containerNode: any = ReactDOM.findDOMNode(this);
      var scroll = containerNode.scrollTop;
      var height = containerNode.offsetHeight;

      var node = e.target;
      var top = node.offsetTop;
      var bottom = top + node.offsetHeight;

      if (bottom > scroll && top < scroll + height) {
        this.props.onMouseEnterResultList(result);
      }
    }
  };

  handleMouseLeave = (e: any) => {
    this.props.onMouseLeaveResultList(null);
  }

  renderResultItem = (result: any) => {
    const focused = this.props.focusedValue
      && this.getResultIdentifier(this.props.focusedValue) === this.getResultIdentifier(result);

    const Renderer = this.props.renderer || ResultItem;

    const { label, valueKey } = this.props;

    return (<Renderer
      ref={focused ? "focused" : undefined}
      key={this.getResultIdentifier(result)}
      valueKey={valueKey}
      result={result}
      focused={focused}
      onMouseEnterResultItem={this.handleMouseEnterResultItem}
      onClickResultItem={this.props.onClickResultItem}
      label={label} />);
  };

  render() {
    var { className, show, results, width } = this.props;

    var style: any = {
      display: show ? 'block' : 'none',
      position: 'absolute',
      listStyleType: 'none',
    };

    if (width) { style.minWidth = `${width}px`; }

    return (
      <ul style={style}
        className={className + " react-autocomplete-Results"}
        onMouseLeave={this.handleMouseLeave}>
        {results.map(this.renderResultItem)}
      </ul>
    );
  }

}

class ResultItem extends React.Component<any, any> {
  static defaultProps = {
    label: function (result: any) {
      return result.title;
    }
  };

  shouldComponentUpdate(nextProps: any) {
    const { valueKey, focused } = this.props;
    return (nextProps.result[valueKey] !== this.props.result[valueKey] ||
      nextProps.focused !== focused);
  }

  getLabel = (result: any) => {
    if (typeof this.props.label === 'function') {
      return this.props.label(result);
    } else if (typeof this.props.label === 'string') {
      return result[this.props.label];
    }
  };

  handleClick = (e: React.MouseEvent<HTMLElement>) => {
    if (this.props.onClickResultItem) {
      this.props.onClickResultItem(e, this.props.result);
    }
  };

  handleMouseEnter = (e: React.MouseEvent<HTMLElement>) => {
    if (this.props.onMouseEnterResultItem) {
      this.props.onMouseEnterResultItem(e, this.props.result);
    }
  };

  render() {
    const activeClass = this.props.focused ? 'react-autocomplete-Result--active' : '';
    return (
      <li
        style={{ listStyleType: 'none' }}
        className={'react-autocomplete-Result ' + activeClass}
        onClick={this.handleClick}
        onMouseEnter={this.handleMouseEnter}>
        <a>{this.getLabel(this.props.result)}</a>
      </li>
    );
  }
}

/**
* Search options using specified search term treating options as an array
* of candidates.
*
* @param {Array.<Object>} options
* @param {String} searchTerm
* @param {Callback} cb
*/
function searchArray(options: any, searchTerm: any, cb: any) {
  if (!options) {
    return cb(null, []);
  }

  searchTerm = new RegExp(searchTerm, 'i');

  var results = [];

  for (var i = 0, len = options.length; i < len; i++) {
    if (searchTerm.exec(options[i].title)) {
      results.push(options[i]);
    }
  }

  cb(null, results);
}

export default Autocomplete;