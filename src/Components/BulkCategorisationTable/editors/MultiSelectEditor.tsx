import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Autocomplete from '../../Autocomplete/Autocomplete';
import * as Fuse from 'fuse.js';
import Utils from '../../../utils/utils';
// const ReactAutocomplete = require('ron-react-autocomplete');
// const { shapes: { ExcelColumn } } = require('react-data-grid');
// const { shapes: { ExcelColumn } } = require('react-data-grid');
// require('../../../../themes/ron-react-autocomplete.css');
// import * as PropTypes from 'prop-types';

// let optionPropType = PropTypes.shape({
// //   id: PropTypes.isRequired,
// //   title: PropTypes.string
// });

class MultiSelectEditor extends React.Component<any, any> {
    //   static propTypes = {
    //     onCommit: PropTypes.func,
    //     options: PropTypes.arrayOf(optionPropType),
    //     label: PropTypes.any,
    //     value: PropTypes.any,
    //     height: PropTypes.number,
    //     valueParams: PropTypes.arrayOf(PropTypes.string),
    //     column: PropTypes.shape(ExcelColumn),
    //     resultIdentifier: PropTypes.string,
    //     search: PropTypes.string,
    //     onKeyDown: PropTypes.func,
    //     onFocus: PropTypes.func,
    //     editorDisplayValue: PropTypes.func
    //   };

    static defaultProps = {
        // resultIdentifier: 'id'
        valueKey: 'id',
        textKey: 'text',
    };

    inputRefs: any[];

    constructor(props: any) {
        super(props);

        this.inputRefs = [];

        this.state = { searchTerms: { ...this.props.value } }
    }

    handleChange = (value: any, key: string) => {
        const searchTermsClone = { ...this.state.searchTerms };
        searchTermsClone[key] = value;

        this.setState({ searchTerms: searchTermsClone });
    }

    getValue = (): any => ({ StretchException: this.state.searchTerms });

    getInputNode = () => {
        return undefined;
        // TODO: use for input focus on cell open
        // const selectedIndex = Number(this.props.getSelectedInputId());
        // if (selectedIndex >= 0) {
        //     return ReactDOM.findDOMNode(this).getElementsByTagName('input')[selectedIndex];
        // }
    };

    // TODO: remove or use for input focus on cell open
    // handleFocus = (index: number) => {
    //     const inputs = ReactDOM.findDOMNode(this).getElementsByTagName('input');
    // }

    render() {
        const { textKey, valueKey, height, options, column, 
            value: values, labels, inputOrderKeys, inputWidth } = this.props;
        const { searchTerms } = this.state;;

        const singleCharLabels = labels.map((l: string) => Utils.selectCharacters(l, 1));

        const pStyle: any = {
            position: "absolute",
            left: 10,
            lineHeight: "30px"
        };
        const wrapperDivStyle: any = {
            paddingTop: 7
        };

        return (
            <div style={wrapperDivStyle}>
                {inputOrderKeys.map((inputKey: any, i: number) => {
                    return <div style={{ position: "relative" }} key={inputKey}>
                        <label style={pStyle}>{singleCharLabels[i]}</label>
                        <Autocomplete
                            style={{
                                maxHeight: height / 4,
                                width: inputWidth,
                                position: "relative",
                                left: 25,
                                marginBottom: 6,
                            }}
                            // onFocus={() => this.handleFocus(key)}
                            // ref={(input) => { this.inputRefs[i] = input }}
                            resultIdentifier={valueKey} // TODO: replace with valueKey
                            initialShowAll={true}
                            label={valueKey}
                            options={options}
                            width={column.width}
                            onChange={(value: any) => this.handleChange(value, inputKey)}
                            valueKey={valueKey}
                            textKey={textKey}
                            value={this.state.searchTerms[inputKey]}
                        />
                    </div>
                })}

            </div>);
    }
}

export default MultiSelectEditor;