import * as React from 'react';
import * as ReactDOM from 'react-dom';

class InputEditor extends React.Component<any, any> {


    static defaultProps = {
        // resultIdentifier: 'id'
        type: 'text',
    };

    input: HTMLInputElement | null;

    constructor(props: any) {
        super(props);
    }

    componentWillMount() {

    }

    getValue(): any {
        const updated: any = {};
        // debugger;
        // @ts-ignore
        updated[this.props.column.key] = this.input.value;
        // updated[this.props.column.key] = this.getInputNode().value;
        // .value;
        return updated;
      }


    // getInputNode() {
    //     return ReactDOM.findDOMNode(this).getElementsByTagName('input')[0];
    // };

    getInputNode() {
        let domNode = ReactDOM.findDOMNode(this);
        // debugger;
        if (domNode.tagName === 'INPUT') {
          return domNode;
        }
    
        return domNode.querySelector('input:not([type=hidden])');
      }

    render() {
        const { value, column, height, onCommit, onCommitCancel, onOverrideKeyDown, rowData, rowMetaData, ...other } = this.props;
        // const { onBlur, value, type, minLength, maxLength } = this.props;

        return (<input ref={(node) => this.input = node} {...other} className="form-control" defaultValue={value}  />);
      }
}   

export default InputEditor;