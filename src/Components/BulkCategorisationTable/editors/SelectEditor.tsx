import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as ReactDataGrid from 'react-data-grid';
import * as ReactDOM from 'react-dom';
import Select from 'material-ui/Select';
import { MenuItem } from 'material-ui/Menu';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';


// import { ReactElement } from 'react';

interface ISelectEditor {
    options: [any]
}

// const ExcelColumnShape = {
//     name: PropTypes.node.isRequired,
//     key: PropTypes.string.isRequired,
//     width: PropTypes.number.isRequired,
//     filterable: PropTypes.bool
//   };

class SelectEditor extends React.Component<any, any> {
    // class SelectEditor extends EditorBase {
    isOpen: boolean;
    static defaultProps = {
        valueKey: 'value',
        textKey: 'text'
    };

    constructor(props: any) {
        super(props);

        const intialValue = this.props.value

        this.state = {
            isOpen: this.props.isOpen,
            value: intialValue
        };
    }

    componentDidMount() {
        // this.setState({ isOpen: true })
    }

    getInputNode() {
        return ReactDOM.findDOMNode(this);
    }


    getValue(): any {
        let updated: any = {};

        const { value } = this.state;

        if (typeof value === 'string') {
            const { options } = this.props;

            // options.filter()
            updated[this.props.column.key] = this.state.value;
        }

        updated[this.props.column.key] = this.state.value;
        console.log(this.state.value)
        // console.log(this.props.options.find((item) => item.Id.toString() === this.state.value);
        return updated;
    }

    //   onClick() {
    //     this.getInputNode().focus();
    //   }

    //   onDoubleClick() {
    //     this.getInputNode().focus();
    //   }

    handleClick = (option: any) => {
        // this.getInputNode().blur();

    }

    handleChange = (event: any) => {
        console.log("HERE", event)
        this.props.giveMeAValue()
        // this.props.onHandleHappened(this.props.myValues[event.target.value])
        this.setState({ value: event.target.value })
    }

    handleClose = (event: any) => {
        this.setState({
            // value: event.target.value,
            isOpen: false
        });
    }

    renderOptions() {
        let optionEl = null;

        const options = this.props.options.map((option: any, i: number) => {
            if (typeof (option) === 'string') {
                optionEl = <option key={option} value={i}>{option}</option>;
            } else {
                const { valueKey, textKey } = this.props;

                optionEl = (
                    <option key={option[valueKey]}
                        value={option[valueKey]} >
                        {option[textKey]}
                    </option>
                );
            }

            return optionEl;
        });

        return options;
    }

    renderSelectOptions() {
        return this.props.options.map((option: any, i: any) => {
            return (
                <MenuItem onClick={(e) => console.log("HOWDY", this)} key={i} value={option[this.props.valueKey]} >
                    {this.props.textKey}
                </MenuItem>
            )

        })
    }
    // : ReactElement<any>
    render() {
        const { rowData, column } = this.props;
        const value = rowData[column.key];

        // map menu items instead
        let Select = null;
        if (this.state.isOpen) {
            Select = <select
                //     style={this.getStyle()}
                defaultValue={this.props.value}
                //     onBlur={this.props.onBlur}
                onChange={this.handleChange} >
                {this.renderOptions()}
            </select>;
        }
        return (
            // <Select
            //     value={this.state.value}
            //     // open={this.state.isOpen}
            //     onChange={this.handleChange}
            //     onClose={this.handleClose} >

            //     {this.renderSelectOptions()}
            //     {/* <MenuItem value={0} >Male</MenuItem>
            //     <MenuItem value={1} >Female</MenuItem> */}
            // </Select>
            <div>
                { Select }
            </div>
            // <List component="nav">
            //     <ListItem button onClick={() => this.handleClick({ Id: 'cheese' })}>
            //         <ListItemText primary="Trash" />
            //     </ListItem>
            //     <ListItem button component="a" href="#simple-list">
            //         <ListItemText primary="Spam" />
            //     </ListItem>
            // </List>

        );
    }
}

export default SelectEditor;