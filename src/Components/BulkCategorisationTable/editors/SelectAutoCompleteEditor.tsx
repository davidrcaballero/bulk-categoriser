import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Autocomplete from '../../Autocomplete/Autocomplete';
import * as Fuse from 'fuse.js';
// const ReactAutocomplete = require('ron-react-autocomplete');
// const { shapes: { ExcelColumn } } = require('react-data-grid');
// const { shapes: { ExcelColumn } } = require('react-data-grid');
// require('../../../../themes/ron-react-autocomplete.css');
// import * as PropTypes from 'prop-types';

// let optionPropType = PropTypes.shape({
// //   id: PropTypes.isRequired,
// //   title: PropTypes.string
// });

// TODO: scroll to initial value result in result list (DC)
// TODO: fix bug change from controlled to uncontrolled input warning with entering text (DC)
// TODO: Throw error if object is used for options without provided optionKey (DC)

class SelectAutoCompleteEditor extends React.Component<any, any> {
    //   static propTypes = {
    //     onCommit: PropTypes.func,
    //     options: PropTypes.arrayOf(optionPropType),
    //     label: PropTypes.any,
    //     value: PropTypes.any,
    //     height: PropTypes.number,
    //     valueParams: PropTypes.arrayOf(PropTypes.string),
    //     column: PropTypes.shape(ExcelColumn),
    //     resultIdentifier: PropTypes.string,
    //     search: PropTypes.string,
    //     onKeyDown: PropTypes.func,
    //     onFocus: PropTypes.func,
    //     editorDisplayValue: PropTypes.func
    //   };

    static defaultProps = {
        valueKey: 'id',
        textKey: 'text',
    };

    autoComplete: any;
    fuse: any;

    constructor(props: any) {
        super(props);

        const config = {
            shouldSort: true,
            threshold: 0.6,
            location: 0,
            distance: 100,
            maxPatternLength: 32,
            minMatchCharLength: 0,
            keys: [props.textKey]
        };

        const optionsArray = Array.isArray(props.options) ? props.options : [];

        this.fuse = new Fuse(optionsArray, config);

        this.state = { firstSearch: true }
    }

    handleChange = () => {
        this.props.onCommit();
    };

    getValue = (): any => {
        const { event, focusedValue, searchTerm, results } = this.autoComplete.state;
        const { column, textKey, value: originalValue, nullable } = this.props;

        const updated: any = { event, focusedValue };
        
        // when we want to accept null
        if (nullable && searchTerm === "" && !this.isFocusedOnSuggestion() && event.key) {
            updated[column.key] = null;

        } else if (this.hasResults() && this.isFocusedOnSuggestion()) {
            updated[column.key] =
                focusedValue;

        } else if ((searchTerm || typeof originalValue === "string") && this.hasResults()) {
            const validSearchTerm = searchTerm !== "" ? searchTerm : originalValue;

            const result = results.find((result: any) => {
                return String(result[textKey]).toLowerCase() === String(validSearchTerm).toLowerCase();
            });

            updated[column.key] = result ? result : results[0];

        } else {
            updated[column.key] = originalValue;
        }

        return updated;
    };

    getInputNode = () => {
        return ReactDOM.findDOMNode(this).getElementsByTagName('input')[0];
    };

    hasResults = (): boolean => {
        return this.autoComplete.state.results.length > 0;
    };

    isFocusedOnSuggestion = (): boolean => {
        return this.autoComplete.state.focusedValue != null;
    };

    constuctValueFromParams = (obj: any, props: Array<string>) => {
        if (!props) {
            return '';
        }

        let ret = [];
        for (let i = 0, ii = props.length; i < ii; i++) {
            ret.push(obj[props[i]]);
        }
        return ret.join('|');
    };

    // searchArray = (options: any, searchTerm: any, cb: Function) => {
    //     const { valueKey, textKey } = this.props;
    //     if (!options) {
    //         return cb(null, []);
    //     }
    //     // debugger
    //     searchTerm = new RegExp(searchTerm, 'i');

    //     var results = [];

    //     for (var i = 0, len = options.length; i < len; i++) {
    //         if (searchTerm.exec(options[i][textKey])) {
    //             results.push(options[i]);
    //         }
    //     }

    //     cb(null, results);
    // }

    // Question (Aili): show all possible results when first opened
    // slightly filter them (i.e. by first word), or show exact result

    /**
     * 
     * show all options when is first search and initialShowAll is true
     * show all options when no search term and no initialValue
     * filter by first character when no search term and initialShowAll is false and value exists
     * filter by search term if it exists
     * @param {options} Array 
     * 
     */

    // show all options when is first search and initialShowAll is true doesn't apply to categorise
    isInitialShowAllEmptySearchTerm = (searchTerm: string) => {
        return !!this.props.initialShowAll && searchTerm === '';
    }

    hasEmptySearchTermOnNextSearch = (searchTerm: string) => {
        return !this.state.firstSearch && searchTerm === '';
    }

    isFirstSearchWithNullInitialValue = () => {
        return this.state.firstSearch && !this.props.value;
    }

    isFirstSearchWithInitialValue = () => {
        return this.state.firstSearch && !!this.props.value;
    }

    // don't be true if we have an initialValue and it's the first search
    // show all options when no search term and no initialValue
    hasInitialValueWithSearchTerm = (searchTerm: any) => {
        return searchTerm !== '' && !!this.props.value;
    }

    searchArray = (options: any, searchTerm: any, cb: Function) => {
        const { valueKey, textKey, value: initialValue, initialShowAll } = this.props;

        if (!options) { return cb(null, []); }

        let results = [];

        if (this.isInitialShowAllEmptySearchTerm(searchTerm)
            || this.hasEmptySearchTermOnNextSearch(searchTerm)
            || this.isFirstSearchWithNullInitialValue()) {
            results = options;

        } else if (!this.hasInitialValueWithSearchTerm(searchTerm)) {
            this.fuse.setCollection(options);

            let searchText = '';

            if (searchTerm) {
                searchText = searchTerm;
            } else {
                searchText = typeof initialValue === 'string' ? initialValue : initialValue[textKey][0];
            }

            results = this.fuse.search(searchText);

        } else {
            this.fuse.setCollection(options);
            results = this.fuse.search(searchTerm);

        }

        if (this.state.firstSearch) { this.setState({ firstSearch: false }); }

        cb(null, results);
    }

    render() {

        const { valueKey, textKey, grid, value,
            onFocus, column, options, resultsWidth, getOptions, rowMetaData } = this.props;


            const displayValue = { [textKey]: value ? value[textKey] : '' }
        const optionsArray = Array.isArray(options) ? options : getOptions(options, rowMetaData);

        const width = resultsWidth || column.width;
        // let label = this.props.label != null ? this.props.label : 'title';
        // <div height={this.props.height} onKeyDown={this.props.onKeyDown}> error with height
        return (<div onKeyDown={this.props.onKeyDown}>
            <Autocomplete
                search={this.searchArray}
                ref={(node: any) => this.autoComplete = node}
                label={textKey}
                width={column.width}
                resultsWidth={width}
                grid={grid()} // TODO: better way to pass parent element to child?
                onChange={this.handleChange}
                onFocus={onFocus}
                resultIdentifier={valueKey}
                textKey={textKey}
                options={optionsArray}
                value={displayValue} 
                />
        </div>);
        // return (<div onKeyDown={this.props.onKeyDown}>
        //     <ReactAutocomplete
        //         search={this.searchArray}
        //         ref={(node: any) => this.autoComplete = node}
        //         label={textKey}
        //         onChange={this.handleChange}
        //         onFocus={this.props.onFocus}
        //         resultIdentifier={valueKey}
        //         options={this.props.options}
        //         value={this.getEditorDisplayValue()} />
        // </div>);
    }
    // render() {
    //     return (
    //         <div>
    //             bob
    //         </div>
    //     )
    // }
}

export default SelectAutoCompleteEditor;