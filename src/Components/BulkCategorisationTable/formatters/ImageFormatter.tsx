import * as React from 'react';// import PropTypes from 'prop-types';
// require('../../../../themes/react-data-grid-image.css');

let PendingPool = {};
let ReadyPool = {};

class ImageFormatter extends React.Component<any, any> {
    //   static propTypes = {
    //     value: PropTypes.string.isRequired
    //   };

    _isMounted: boolean;

    state = {
        ready: false
    };

    componentWillMount() {
        this._load(this.props.value);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    componentWillReceiveProps(nextProps: any) {
        if (nextProps.value !== this.props.value) {
            this.setState({ value: null });
            this._load(nextProps.value);
        }
    }

    _load = (src: string) => {
        let imageSrc = src;
        // @ts-ignore
        if (ReadyPool[imageSrc]) {
            this.setState({ value: imageSrc });
            return;
        }

        // @ts-ignore

        if (PendingPool[imageSrc]) {
            // @ts-ignore

            PendingPool[imageSrc].push(this._onLoad);
            return;
        }

        // @ts-ignore

        PendingPool[imageSrc] = [this._onLoad];

        let img = new Image();
        img.onload = () => {
            // @ts-ignore

            PendingPool[imageSrc].forEach(callback => {
                callback(imageSrc);
            });
            // @ts-ignore

            delete PendingPool[imageSrc];
            img.onload = null;
            // @ts-ignore

            imageSrc = undefined;
        };
        img.src = imageSrc;
    };

    _onLoad = (src: string) => {
        if (this._isMounted && src === this.props.value) {
            this.setState({
                value: src
            });
        }
    };

    render() {
        // @ts-ignore
        // .react-grid-image {
        //     background: #efefef;
        //     background-size: 100%;
        //     display: inline-block;
        //     height: 40px;
        //     width: 40px;
        //   }
        const { width, height } = this.props;

        // @ts-ignore

        let style = this.state.value ?

            {
                // @ts-ignore
                backgroundImage: `url(${this.state.value})`,
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center',
                height,
                width
            } :
            undefined;

        return <div className="react-grid-image" style={style} />;
    }
}


export default ImageFormatter;