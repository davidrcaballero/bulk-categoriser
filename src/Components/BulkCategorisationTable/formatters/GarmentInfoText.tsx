import * as React from 'react';
import * as PropTypes from 'prop-types';
import {render} from 'react-dom'


const divStyle = {
    listStyleType: 'none',
    margin:'0',
    padding:'0',


  };

class GarmentInfoText extends React.Component<any, any> {

    render() {
        const { classes } = this.props;
        const {column, dependentValues } = this.props;
        const { ArticleNumber, BrandName, ClientName, Title, FabricComposition } = this.props.dependentValues;
        //console.log('TITLETITLETITLETITLETITLETITLE ',BrandName);
        return(
            <div >
                <div style={divStyle}>
                    <div><p style={{ textOverflow: "ellipsis", overflow: "hidden", whiteSpace: "pre", padding:"0", margin:"0" }}>{ArticleNumber}</p></div> 
                    <div><p style={{ textOverflow: "ellipsis", overflow: "hidden", whiteSpace: "pre", padding:"0", margin:"0" }}>{ClientName}</p></div>
                    <div><p style={{ textOverflow: "ellipsis", overflow: "hidden", whiteSpace: "pre", padding:"0", margin:"0" }}>{BrandName}</p></div>
                    <div><p style={{ textOverflow: "ellipsis", overflow: "hidden", whiteSpace: "pre", padding:"0", margin:"0" }}>{Title}</p></div>
                    <div><p style={{ textOverflow: "ellipsis", overflow: "hidden", whiteSpace: "pre", padding:"0", margin:"0" }}>{FabricComposition}</p></div>
                </div>
            </div>
        );
    }
}

export default GarmentInfoText;
