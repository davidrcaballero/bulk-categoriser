import * as React from 'react';
import * as PropTypes from 'prop-types';
import Utils from '../../../utils/utils';

class MultiSelectFormatter extends React.Component<any, any> { //TODO: TS props

  static defaultProps = {
    valueKey: "id",
    textKey: "name",
  };

  constructor(props: any) {
    super(props);
  }

  // static propTypes = {
  //   options: 
  //     // PropTypes.oneOfType([
  //       // PropTypes.string,
  //       PropTypes.shape({
  //         Id: PropTypes.string,
  //         Name: PropTypes.string
  //         // title: PropTypes.string,
  //         // value: PropTypes.string,
  //         // text: PropTypes.string
  //       }).isRequired,
  //   // value: PropTypes.string.isRequired
  // };

  // static propTypes = {
  //   value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object, PropTypes.bool]).isRequired
  // };

  shouldComponentUpdate(nextProps: any): boolean {
    return nextProps.value !== this.props.value;
  }

  render() {
    const { value: values, textKey, valueKey, labels, height, inputOrderKeys, inputWidth } = this.props;

    const singleCharLabels = labels.map((l: string) => Utils.selectCharacters(l, 1));

    const pStyle: any = {
      position: "absolute",
      left: 0,
      lineHeight: "30px"
    };

    return (
      <div>
        {inputOrderKeys.map((inputKey: any, i: number) => {
          return (<div style={{ position: "relative" }} key={inputKey}>
            <label style={pStyle}>{singleCharLabels[i]}</label>
            <input
              style={{
                maxHeight: "30px",
                width: inputWidth,
                position: "relative",
                left: 25,
                marginBottom: 7,
                padding: 4,
                border: "none",
                WebkitBoxShadow: "none",
                boxShadow: "none"
              }}
              data-input-index={i}
              readOnly
              value={values[inputKey][textKey]} />
          </div>)
        })}

      </div>);
  }
}

export default MultiSelectFormatter;