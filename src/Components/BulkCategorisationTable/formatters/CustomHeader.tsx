import * as React from 'react';
import * as mui from 'material-ui';
import * as PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';

const styles = (theme:any) => ({
    
    rowcount: {
        paddingBottom: '10px', 
        color:'#83209c',  
    },
    flex: {
        color:'#83209c',
        fontSize: '0.9rem',
        textTransform:'lowercase',
        '&:hover': {
            backgroundColor:'rgba(0, 0, 0, 0)',
            fontWeight:'bold',
        },
    },
    btntitle: {
        top:'0px',
        left: '-14px',
        width:'50px',
        minWidth:'50px',
        padding: '0.5rem',

        minHeight: '26px',

        '&:hover': {
            backgroundColor:'rgba(0, 0, 0, 0)',
            fontWeight:'bold',
        },
    },
    btngarmentid: {
        top:'0px',
        left: '-8px',
        width:'50px',
        minWidth:'50px',
        padding: '0.5rem',
        minHeight: '26px',

        '&:hover': {
            backgroundColor:'rgba(0, 0, 0, 0)',
            fontWeight:'bold',
        },
    },
    root: {
        width: '200px',
        'z-index':'1',
        backgroundColor:'transparent',
    },
    header: {
        fontFamily: 'Helvetica Neue",Helvetica,Arial,sans-serif',
        fontSize: '0.9rem',
        top:'0px',
        left:'0px',
    },
});

const theme = createMuiTheme({
    palette: {
        primary: { main: '#83209c' },
        secondary: { main: '#c7c7c7' },
    },
    typography: {
        fontFamily: 'Pathway Gothic One',
        fontSize: '21px',
    },
});
const divStyle = {
    position:'absolute',
  };

class CustomHeader extends React.Component<any, any> {
    constructor(props:any) {
        super(props);
  
      }
      static propTypes = {
        classes: PropTypes.object.isRequired,
  
      }
      onChangeHandler() {
          console.log('the btn was triggered')
      }
    render() {
        const { classes, handleGridSort } = this.props;
        return(
         <div style={{position:'absolute'}} className={classes.root}>
                 <div style={{fontWeight:'bold'}}  className={classes.header}>Garment Info</div>
                 <Button style={{position:'relative'}} className={classes.btngarmentid} onClick={() => this.props.handleGridSort("ArticleNumber", "ASC")} onChange={this.onChangeHandler}>
                   <Typography className={classes.flex}>By-id</Typography>
                 </Button>
                 <Button style={{position:'relative'}} className={classes.btntitle} onClick={() => this.props.handleGridSort("Title", "DESC")} onChange={this.onChangeHandler}>
                   <Typography className={classes.flex}>By-title</Typography>
                 </Button>
         </div>
        ); 
      }
}
export default withStyles(styles)(CustomHeader);
