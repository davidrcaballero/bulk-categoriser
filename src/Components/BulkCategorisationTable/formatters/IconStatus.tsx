import * as React from 'react';
import * as PropTypes from 'prop-types';

class IconStatus extends React.Component<any, any> {

    render() {
        const { Status } = this.props.dependentValues;
        const IconGreen = ( {fill}:any) => (
            <div style={{left:'0px', marginBottom:'33px', position:'relative'}}>
                <svg fill={ fill } height="43" viewBox="0 0 43 43" width="43" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 0h24v24H0z" fill="none"/>
                    <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"/>
                </svg>
            </div>
          );
          const IconRed = ( {fill}:any) => (
            <div style={{left:'0px', marginBottom:'33px', position:'relative'}}>
                <svg fill={ fill } height="43" viewBox="0 0 43 43" width="43" xmlns="http://www.w3.org/2000/svg">
                    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
                    <path d="M0 0h24v24H0z" fill="none"/>
                </svg>
            </div>
 
          );
        return(
            Status.Name === "Categorized" ? <IconGreen fill="#3f9186" /> : <IconRed fill="#c24440" />
        );
    }
}

export default IconStatus;
  
