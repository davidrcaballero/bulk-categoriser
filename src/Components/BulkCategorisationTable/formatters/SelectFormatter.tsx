import * as React from 'react';
import * as PropTypes from 'prop-types';

class SelectFormatter extends React.Component<any, any> { //TODO: TS props

  static defaultProps = {
    valueKey: "id",
    textKey: "name",
  };


  constructor(props: any) {
    super(props);
  }

  // static propTypes = {
  //   options: 
  //     // PropTypes.oneOfType([
  //       // PropTypes.string,
  //       PropTypes.shape({
  //         Id: PropTypes.string,
  //         Name: PropTypes.string
  //         // title: PropTypes.string,
  //         // value: PropTypes.string,
  //         // text: PropTypes.string
  //       }).isRequired,
  //   // value: PropTypes.string.isRequired
  // };

  // static propTypes = {
  //   value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object, PropTypes.bool]).isRequired
  // };

  shouldComponentUpdate(nextProps: any): boolean {
    return nextProps.value !== this.props.value;
  }

  render() {
    const { value, textKey, valueKey } = this.props;

    return (
      <React.Fragment>
        {value ? <div title={value[textKey]}>{value[textKey]}</div>
         : null}
      </React.Fragment>
    );
  }
}

export default SelectFormatter;