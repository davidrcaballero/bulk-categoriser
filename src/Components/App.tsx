import * as React from 'react';
import * as mui from 'material-ui';
import { HashRouter, Route, Switch, withRouter, Redirect } from 'react-router-dom';
// import { Router } from 'react-router-dom'
// <Router history={history}> <Router/>
import FashionIq from './Pages/FashionIq';
import FitOrigin from './Pages/FitOrigin';
import SupportCentre from './Pages/SupportCentre';
import BulkCategorisationTable from './Pages/BulkCategorisationTable';
import ManageClients from './Pages/ManageClients';
import Account from './Pages/Account';
import CustomeAppBar from './AppBar/CustomAppBar';
import { createHashHistory } from 'history';
// import BulkCategorisationTable from './BulkCategorisationTable/BulkCategorisationTable';
// import BulkCategorisationTableList from './BulkCategorisationTable/BulkCategorisationTable';
// import BulkCategorisationTable from '';
class App extends React.Component<any, any> {

    render() {
        return (
            <div>
                <CustomeAppBar/>
                {/* <HashRouter history={createHashHistory()}> */}
                <Switch>
                    <Route path="/fashionqi" component={FashionIq} />
                    <Route path="/fitorigin" component={FitOrigin} />
                    <Route path="/supportcentre" component={SupportCentre} />
                    <Route path="/manageclients" component={ManageClients} />
                    <Route path="/account" component={Account} />
                    <Route path="/" component={BulkCategorisationTable} />
                    <Redirect to="/" />
                </Switch>
                {/* </HashRouter> */}
            </div>
        )
    }
}

export default App;
